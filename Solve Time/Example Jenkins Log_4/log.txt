{"date": "2016-06-30 07:06AM", "message": "**************************************************************", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "SOLVER VERSION: 5.1.7", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "**************************************************************", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Detected the provided cache as FDH->FAP->MST->NIU", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Finding drop demand and dedicated demand", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "**************************************************************", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Solving for Distribution cables", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "**************************************************************", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Starting process with PID 16", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Footprint provided with 1668 edges", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Candidate hub at 01010000005BE42ACE8A402341C8D0024B892C5041 with illegal TYPE RESERVED_UG, proceeding anyway", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "edge ('0101000000954B84EBED2223419163CF2CE8275041', '0101000000998E48E1D2222341CB943543F1275041', 0) has two owners HUB_01010000009ABE433161202341ADA9AC3B89275041 AND HUB_010100000006198B42C22323411ABDCE2CD6275041", "severity": "WARNING"}
{"date": "2016-06-30 07:06AM", "message": "edge ('010100000054DC2A5C0B23234169450B47DE275041', '0101000000954B84EBED2223419163CF2CE8275041', 0) has two owners HUB_01010000009ABE433161202341ADA9AC3B89275041 AND HUB_010100000006198B42C22323411ABDCE2CD6275041", "severity": "WARNING"}
{"date": "2016-06-30 07:06AM", "message": "edge ('01010000000E89E51E372323416CFC9C61CF275041', '0101000000803FF42832232341105CB95ED1275041', 0) has two owners HUB_01010000009ABE433161202341ADA9AC3B89275041 AND HUB_010100000006198B42C22323411ABDCE2CD6275041", "severity": "WARNING"}
{"date": "2016-06-30 07:06AM", "message": "edge ('0101000000803FF42832232341105CB95ED1275041', '010100000054DC2A5C0B23234169450B47DE275041', 0) has two owners HUB_01010000009ABE433161202341ADA9AC3B89275041 AND HUB_010100000006198B42C22323411ABDCE2CD6275041", "severity": "WARNING"}
{"date": "2016-06-30 07:06AM", "message": "Final solve with 1 hubs", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Creating Parameters", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Creating Problem", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Validating Problem", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Solving Problem", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Calling Distribution Solver with splice constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Formulating MIP", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "There are 814 flow variables.", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "There are 814 arc use variables.", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding non overlapping hub area constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding flow conservation constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding hub capacity constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding serve demand constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding arc capacity constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding arc in one direction used only constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding splice constraints", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Adding objective function", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Solving MIP", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Optimize a model with 2187 rows, 1628 columns and 4931 nonzeros", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Coefficient statistics:", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "  Matrix range    [1e+00, 4e+02]", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "  Objective range [8e+01, 1e+05]", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "  Bounds range    [1e+00, 4e+02]", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "  RHS range       [1e+00, 1e+04]", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Presolve removed 2187 rows and 1628 columns", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Presolve time: 1.57s", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Presolve: All rows and columns removed", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "\nExplored 0 nodes (0 simplex iterations) in 1.74 seconds", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Thread count was 1 (of 16 available processors)", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Optimal solution found (tolerance 1.00e-01)", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Best objective 6.511827894389e+06, best bound 6.511827894389e+06, gap 0.0%", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Recovering solution", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Creating Solution", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Validating Solution", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "**************************************************************", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Demands: 18156", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Drop terminals(MSTs): 3856", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "MST Packing Ratio: 4.70850622407", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "NAPs(FAPs): 640", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "MST Demand: 715", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "FAP Packing Ratio: 7.1421875", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "af_fap_demand: 3", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "HUBs(FDHs): 62", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "FDH Packing Ratio: 10.3709677419", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Updating Google Spec", "severity": "INFO"}
{"date": "2016-06-30 07:06AM", "message": "Updating Google Spec for 'hub_type' HUT", "severity": "INFO"}
