#######################################################################
#######################################################################
### GETTING DATA FROM WALDO-CONSULTANTS ###

#directory is /var/whale/data/pairtree_root

import os, json
import csv
import re
from datetime import datetime
directory = r'/var/whale/data/pairtree_root'
#directory = r'/var/whale/data_kingkong1/pairtree_root'
# jobid = []
# solve_finish = []
# user = []
# submit_time = []
# version = []
# last_mod_time = []
# solve_time_seconds = []
def get_log(data):
    some_info1 = {}    

    Demand = MSTs = MST_packing = FAPs = FAP_packing = FDHs = FDH_packing = Dist = mstdemand = affapdemand  = 'No Run'
    for i in data[-12:-1]:

        #xjs = i.replace('"', '\"')
        xjs = re.sub(',\n','',i)
	get_tier = json.loads(xjs)
        tier_msg = get_tier.get('message','No info')
        demandcount = re.split("Demands: ", tier_msg)
        mstcount = re.split("Drop terminals\(MSTs\): ", tier_msg)
        fapcount = re.split("NAPs\(FAPs\): ", tier_msg)
        fdhcount = re.split("HUBs\(FDHs\): ", tier_msg)
        mstpack = re.split("MST Packing Ratio: ", tier_msg)
        fappack = re.split("FAP Packing Ratio: ", tier_msg)
        fdhpack = re.split("FDH Packing Ratio: ", tier_msg)
        mstdemandc = re.split("MST Demand: ", tier_msg)
        affapdemandc = re.split("af_fap_demand: ", tier_msg)        
        if len(demandcount) > 1:
            Demand = demandcount[1]

        if len(mstcount) > 1:
            MSTs = mstcount[1]

        if len(mstpack) > 1:
            MST_packing = mstpack[1]
        #    MSTs = 'No Run'
        #    MST_packing = 'No Run' 
        if len(fapcount) > 1:
            FAPs = fapcount[1]
        
        if len(fappack)> 1:
            FAP_packing = fappack[1]
        #else:
        #    FAPs = 'No Run'
        #    FAP_packing = 'No Run'
        if len(fdhcount) > 1:
            FDHs = fdhcount[1]
        if len(fdhpack) > 1:
            FDH_packing = fdhpack[1]
	if len(mstdemandc) > 1:
	    mstdemand = mstdemandc[1]
	if len(affapdemandc) > 1:
	    affapdemand = affapdemandc[1]
        #else:
        #    FDHs = 'No Run'
        #    FDH_packing = 'No Run'

    
    # packtier = re.split(':?', tier_msg)
    # performtier = re.split("Not performing", tier_msg)
    # if len(packtier) == 1 and len(performtier) > 1:
        # tier_packing = 'No Run'
    # elif len(packtier) == 2:
        # tier_packing = packtier[1]
    # else:
        # tier_packing = 'No info'
    # some_info.append(tier_packing
    for last_line in data[-5:]:
    #last_line = data[-1]
        #ljs = last_line.replace('"', '\"')
	ljs = re.sub(',\n','',last_line)
    	lldict = json.loads(ljs)
    	ll_msg = lldict.get("message",'No info')
    	if ll_msg[-3:] == 'HUT':
            Dist = 'Yes'
    
    cof = 'No info'
    for cf in data[:10]:        
        #cfj = cf.replace('"', '\"')
	cfj = re.sub(',\n','',cf)
        cachefp = json.loads(cfj)
        cf_msg = cachefp.get("message",'No info')
        
        cf_use = re.split('Detected the provided cache as ',cf_msg)
        if len(cf_use) ==2:            
            if re.search('no solution', cf_msg):
                cof = 'Footprint'
            else:
                cof = 'Cache'
        
        
    some_info1["Demand"] = Demand
    some_info1["MSTs"] = MSTs
    some_info1["MST_packing"] = MST_packing
    some_info1["FAPs"] = FAPs
    some_info1["FAP_packing"] = FAP_packing
    some_info1["FDHs"] = FDHs
    some_info1["FDH_packing"] = FDH_packing
    some_info1["Dist"] = Dist
    some_info1["COF"] = cof
    some_info1["MST_Demand"] = mstdemand
    some_info1["AF_FAP_Demand"] = affapdemand    
    return some_info1
    


some_dict = {}
csvline = []
csvline.append('jobid,solve_finish,user,submit_time,version,last_modified,run_time,Demand,MSTs, \
MST_packing,FAPs,FAP_packing,FDHs,FDH_packing,Dist,COF, Arc_AER, Length_AER, Arc_UG, Length_UG, \
use_hub_balancing,cable_cost_mst,max_drop_length_ar,reuse_factor, min_fdh_to_use, prune_distribution_input_design_graph,\
max_drop_length_ug,run_fdh_packing,hub_installation_cost_mst, fdh_max_solver_duration_1_hub_packing, run_nap_packing,\
run_distribution_solver, treat_hubs_as, max_candidate_connections_mst, mip_stop_gap_fdh, mip_stop_gap_nap, mip_stop_gap_dist,\
mip_stop_gap_mst,discount_preference_value, access_cabling_algo, MST_Demand, AF_FAP_Demand')
id_dir = 0
for dirpath,_,filenames in os.walk(directory):
#for id, (dirpath,_,filenames) in enumerate(os.walk(directory)):
    some_info = {}
    for f in filenames:
        if f == 'stat.json':
            with open(os.path.abspath(os.path.join(dirpath,f))) as ff:
                info1 = json.load(ff)
                print info1   # checked, only contains job_id and status #
                #jobid.append(json.load(ff).get('job_id'))
                some_info["jobid"] = info1.get('job_id', 'No info')
        elif f == 'server_attributes.json':		
            with open(os.path.abspath(os.path.join(dirpath,f))) as serv_att:
                info2 = json.load(serv_att)
                print info2    # checked, only contains worker_finished #
                #solve_finish.append(json.load(serv_att).get('worker_finished'))
                some_info["solve_finish"] = info2.get('worker_finished','No info')
        elif f == 'usage.json':
            with open(os.path.abspath(os.path.join(dirpath,f))) as use:                 
                #print json.load(use).keys()  # Contains solver, submitted, version, and user #
                use_dict = json.load(use)
                #user.append(use_dict.get('user'))
                #submit_time.append(use_dict.get('submitted'))
                #version.append(use_dict.get('version'))
                some_info["user"] = use_dict.get('user','No info')
                some_info["submit_time"] = use_dict.get('submitted','No info')
                some_info["version"] = use_dict.get('version','No info')
        elif f == 'persisted_state.json':
            with open(os.path.abspath(os.path.join(dirpath,f))) as per_state:
                p1 = json.load(per_state)
                print p1.get('solution.json','No info')
		if 'solution.json' in p1:
                #some_info["last_modified"] = json.load(persist).get('solution.json','No info').get('_last_modified','No info')
                    some_info["last_modified"] = p1.get('solution.json','No info').get('_last_modified','No info')
    	
        elif f == 'log.txt':
	    with open(os.path.abspath(os.path.join(dirpath,f))) as f1:
            	x = f1.readlines()
            	logdata = get_log(x)
            	some_info["Demand"] = logdata.get("Demand", 'No info')
            	some_info["MSTs"] = logdata.get("MSTs", 'No info')
            	some_info["MST_packing"] = logdata.get("MST_packing", 'No info')
            	some_info["FAPs"] = logdata.get("FAPs", 'No info')
            	some_info["FAP_packing"] = logdata.get("FAP_packing", 'No info')
            	some_info["FDHs"] = logdata.get("FDHs", 'No info')
            	some_info["FDH_packing"] = logdata.get("FDH_packing", 'No info')
            	some_info["Dist"] = logdata.get("Dist", 'No info')
            	some_info["COF"] = logdata.get("COF", 'No info')
                some_info["MST_Demand"] = logdata.get("MST_Demand", 'No info')
                some_info["AF_FAP_Demand"] = logdata.get("AF_FAP_Demand", 'No info')
        elif f == 'input_data.json':
            with open(os.path.abspath(os.path.join(dirpath,f))) as input:
                x1 = json.load(input)
                # candidate graph #
                for key0 in x1.keys():
		    if re.search('candidate_graph',key0):
			cand_graph = x1.get(key0)
		    #else:
			#import pdb ; pdb.set_trace()
		#cand_graph = x1.get('candidate_graph','No info')
                #if some_info['jobid'] == '2babefe9-fd1c-4a3e-8024-182435235c6e':
		#	import pdb; pdb.set_trace()

		# links contain useful information #
    		    elif re.search('args', key0):
        	    	args = x1.get(key0)
        	    	for key in args.keys(): 
            		    if re.search('use_hub_balancing',key):
                	    	hub_balance = args.get(key)
            		    elif re.search('cable_cost_mst',key):
                	    	cable_cost_mst = args.get(key)
            		    elif re.search('max_drop_length_ar',key):
                	    	drop_length_aer = args.get(key)
            		    elif re.search('max_drop_length_ug',key):
                	    	drop_length_ug = args.get(key)
            		    elif re.search('reuse_factor',key):
                	    	reuse_factor = args.get(key)
            		    elif re.search('min_fdh_to_use',key):
                	    	min_fdh = args.get(key)
            		    elif re.search('prune_distribution_input_design_graph',key):
                    	    	prune_dist = args.get(key)
            		    elif re.search('run_fdh_packing',key):
                	    	run_fdh = args.get(key)
            		    elif re.search('hub_installation_cost_mst',key):
                	    	mst_hub_cost = args.get(key)
            		    elif re.search('fdh_max_solver_duration_1_hub_packing',key):
                	    	fdh_max_1_hub = args.get(key)
            		    elif re.search('run_nap_packing',key):
                	    	run_fap = args.get(key)
            		    elif re.search('run_distribution_solver',key):
                	    	run_dist = args.get(key)
            		    elif re.search('treat_hubs_as',key):
                	    	treat_hubs_as = args.get(key)
            		    elif re.search('max_candidate_connections_mst',key):
                	    	max_candidate_connections_mst = args.get(key)
            		    elif re.search('mip_stop_gap_fdh',key):
                	    	mip_fdh = args.get(key)
            		    elif re.search('mip_stop_gap_nap',key):
                	    	mip_fap = args.get(key)
            		    elif re.search('mip_stop_gap_dist',key):
                	    	mip_dist = args.get(key)
            		    elif re.search('mip_stop_gap_mst',key):
                	    	mip_mst = args.get(key)
            		    elif re.search('discount_preference_value',key):
                	    	discount_preference = args.get(key)
            		    elif re.search('access_cabling_algo',key):
                	    	cabling_algo = args.get(key)
                cg_links = cand_graph.get("links",'No info')

                num_aer = num_ug = len_aer = len_ug = 0
                for arc in cg_links:
                    if arc['TYPE'] == 'UG':
                        num_ug += 1
                        len_ug += arc['LENGTH']
                    elif arc['TYPE']== 'AER':
                        num_aer += 1 
                        len_aer += arc['LENGTH']
                some_info["Length_UG"] = len_ug
                some_info["Length_AER"] = len_aer
                some_info["Arc_UG"] = num_ug
                some_info["Arc_AER"] = num_aer
                some_info["use_hub_balancing"] = hub_balance
                some_info["cable_cost_mst"] = cable_cost_mst
                some_info["max_drop_length_ar"] = drop_length_aer 
                some_info["max_drop_length_ug"] = drop_length_ug
                some_info["reuse_factor"] = reuse_factor
                some_info["min_fdh_to_use"] = min_fdh
                some_info["prune_distribution_input_design_graph"] = prune_dist
                some_info["run_fdh_packing"] = run_fdh
                some_info["hub_installation_cost_mst"] = mst_hub_cost
                some_info["fdh_max_solver_during_1_hub_packing"] = fdh_max_1_hub
                some_info["run_nap_packing"] = run_fap
                some_info["run_distribution_solver"] = run_dist
                some_info["treat_hubs_as"] = treat_hubs_as
                some_info["max_candidate_connections_mst"] = max_candidate_connections_mst
                some_info["mip_stop_gap_fdh"] = mip_fdh
                some_info["mip_stop_gap_nap"] = mip_fap
                some_info["mip_stop_gap_dist"] = mip_dist
                some_info["mip_stop_gap_mst"] = mip_mst
                some_info["discount_preference_value"] = discount_preference
                some_info["access_cabling_algo"] = cabling_algo

    if 'submit_time' in some_info and 'last_modified' in some_info:				 
        st_time = datetime.strptime(some_info["submit_time"], '%Y-%m-%d %H:%M:%S') #example u'2016-03-02 23:06:31'
        en_time = datetime.strptime(some_info["last_modified"],'%Y-%m-%dT%H:%M:%S')
        run_time = (en_time - st_time).total_seconds()
        some_info["run_time"] = run_time
	    
    if some_info:
        some_dict[id_dir] = some_info												
        line = str(some_info.get('jobid','No info')) + ',' +str(some_info.get('solve_finish','No info'))+','+\
str(some_info.get('user','No info')) +',' + str(some_info.get('submit_time','No info')) + ',' +\
str(some_info.get('version','No info')) + ',' + str(some_info.get('last_modified','No info')) + ',' +\
str(some_info.get('run_time','No info'))+ ',' + str(some_info.get('Demand','No info')) + ',' +\
str(some_info.get('MSTs','No info'))+ ',' +str(some_info.get('MST_packing','No info')) + ',' +\
str(some_info.get('FAPs','No info'))+ ',' + str(some_info.get('FAP_packing','No info')) + ',' +\
str(some_info.get('FDHs','No info'))+ ',' + str(some_info.get('FDH_packing','No info')) + ',' +\
str(some_info.get('Dist','No info'))+ ',' + str(some_info.get('COF','No info')) +',' +\
str(some_info.get('Arc_AER','No info'))+ ',' + str(some_info.get('Length_AER','No info')) + ',' +\
str(some_info.get('Arc_UG','No info'))+ ',' + str(some_info.get('Length_UG','No info')) + ',' +\
str(some_info.get('use_hub_balancing','No info'))+ ',' + str(some_info.get('cable_cost_mst','No info'))  + ',' +\
str(some_info.get('max_drop_length_ar','No info'))+ ',' + str(some_info.get('reuse_factor','No info'))  + ',' +\
str(some_info.get('min_fdh_to_use','No info'))+ ',' + str(some_info.get('prune_distribution_input_design_graph','No info'))  + ',' +\
str(some_info.get('max_drop_length_ug','No info'))+ ',' +str( some_info.get('run_fdh_packing','No info'))  + ',' +\
str(some_info.get('hub_installation_cost_mst','No info'))+ ',' + str(some_info.get('fdh_max_solver_duration_1_hub_packing','No info'))  + ',' +\
str(some_info.get('run_nap_packing','No info'))+ ',' + str(some_info.get('run_distribution_solver','No info'))  + ',' +\
str(some_info.get('treat_hubs_as','No info'))+ ',' + str(some_info.get('max_candidate_connections_mst','No info'))  + ',' +\
str(some_info.get('mip_stop_gap_fdh','No info'))+ ',' + str(some_info.get('mip_stop_gap_nap','No info'))  + ',' +\
str(some_info.get('mip_stop_gap_dist','No info'))+ ',' + str(some_info.get('mip_stop_gap_mst','No info'))  + ',' +\
str(some_info.get('discount_preference_value','No info'))+ ',' + str(some_info.get('access_cabling_algo','No info')) + ',' +\
str(some_info.get('MST_Demand','No info'))+ ',' + str(some_info.get('AF_FAP_Demand','No info')) 
        csvline.append(line)                  
    id_dir = id_dir + 1


output_file = '/home/chang_liu/Waldo_With_Input_Parameters.csv'
with open(output_file,'w') as fun:
    fun.write('\n'.join(csvline))    
