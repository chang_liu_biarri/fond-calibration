"""I never wrote any PYTHON script before. 
This is the very first one, on Regualar Expressions."""


### Read in a json file to see if it works ###
import json
from datetime import datetime
import numpy
import re 

# usage.json is a dictionary #
# It contains the solver, submission date, version of the solver, and user. #
with open('C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\usage.json') as json_data:
 print(json.load(json_data)["user"])
 print json_data
 print json_data["user"]
 print(json.load(json_data)["user"])
 print(json.load(json_data)["user"])
 print(json.load(json_data).keys())
 print(json.load(json_data).keys())
 print(json.load(json_data).keys())
 print(json.load(json_data))
 usage = json.load(json_data)
 print(usage)
	
# stat.json is a dictionary #
# It contains status (whether the task is successful), as well as the job_id. #
# MIGHT NOT NEED TO LOAD #
with open('C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\stat.json') as json_data_stat:
 statjson = json.load(json_data_stat)
 print(statjson)

# server_attributes.json is a dictionary #
# It contains work_finished, solve_start_time, solve_finish_time. We might just use this
# to calculate the time taken. #
with open('C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\server_attributes.json') as json_data_sa:
 sajson = json.load(json_data_sa)
 print(sajson)

# Save status of the solve #
solve_status = sajson.get('worker_finished')
# Save version of the solver #
FOND_version = usage.get('version')
# Save user #
solve_user = usage.get('user')
# Calculate time taken, solve_finish_time - solve_start_time, then save #
start_time = sajson.get('solve_start_time')
st_date = datetime.strptime(start_time,'%Y-%m-%d %H:%M:%S')
end_time = sajson.get('solve_finish_time')
et_date = datetime.strptime(end_time,'%Y-%m-%d %H:%M:%S')
run_time = (et_date - st_date).total_seconds()
# Calculate solve waiting time, the save
queue_time = datetime.strptime(sajson.get('solve_submission_time'),'%Y-%m-%d %H:%M:%S')
qu_time = (st_date - queue_time).total_seconds()


### Create a matrix that contains the solver information ###

Mat_Colnames = ['Solve_Status','FOND_Version','Solve_User','Run_Time','Queue_Time']
Info_Mat = [solve_status,FOND_version,solve_user,run_time,qu_time]
### Save a matrix to csv file ###
#a = np.asarray([ [1,2,3], [4,5,6], [7,8,9] ])
#a.tofile('foo.csv',sep=',',format='%10.5f')



#######################################################################
#######################################################################
### GETTING DATA FROM WALDO-CONSULTANTS ###

#directory is /var/whale/data/pairtree_root

import os, json

directory = r'/var/whale/data/pairtree_root'

for dirpath,_,filenames in os.walk(directory):
  for f in filenames:
	  if f == 'stat.json':
		  with open(os.path.abspath(os.path.join(dirpath,f))) as f:
			  print json.load(f)['job_id']



### Chang2.py ###
import os, json

directory = '/the/dir/data/pairtree_root'

for dirpath,_,filenames in os.walk(directory):
    for f in filenames:
        if f == 'stat.json':
            with open(os.path.abspath(os.path.join(dirpath, f))) as f:
                import pdb; pdb.set_trace()
                print 'cool'


with open(os.path.join(dirpath, 'server_attributes.json')) as serv_att: print json.load(serv_att).keys()

print os.system('ls ' + dirpath)



### HITTING ERROR? ###
python -m pdb chang2.py



### READ IN txt file ###
import json
import re
# from numpy import loadtxt

# lines = loadtxt("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_2\log.txt", delimiter = '}', unpack = False)


# f = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_2\log.txt", 'r')
f = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\log.txt", 'r')
x = f.readlines()
y = str(x)
xjs = x[-6].replace('"', '\"')
get_mst = json.loads(xjs)
mst_msg = get_mst.get('message','No info')

mst_packing = re.split(":?",mst_msg)[1]

xjs_fap =  x[-5].replace('"', '\"')
get_fap = json.loads(xjs_fap)
fap_msg = get_fap.get('message','No info')

# len(re.split(":?",fap_msg)) == 2

# len(re.split("Not performing", fap_msg)) > 1

packfap = re.split(":?",fap_msg)
performfap = re.split("Not performing", fap_msg)
if len(packfap) == 1 and len(performfap) > 1:
    fap_packing = 'No Run'
elif len(packfap) == 2:
    fap_packing = packfap[1]
else:
    fap_packing = 'No info'

    


def get_tier_info(data):
    some_info = {}    

    Demand = MSTs = MST_packing = FAPs = FAP_packing = FDHs = FDH_packing = 'No Run'
    for i in x[-12:-1]:

        xjs = i.replace('"', '\"')
        get_tier = json.loads(xjs)
        tier_msg = get_tier.get('message','No info')
        demandcount = re.split("Demands: ", tier_msg)
        mstcount = re.split("Drop terminals\(MSTs\): ", tier_msg)
        fapcount = re.split("NAPs\(FAPs\): ", tier_msg)
        fdhcount = re.split("HUBs\(FDHs\): ", tier_msg)
        mstpack = re.split("MST Packing Ratio: ", tier_msg)
        fappack = re.split("FAP Packing Ratio: ", tier_msg)
        fdhpack = re.split("FDH Packing Ratio: ", tier_msg)
        
        if len(demandcount) > 1:
            Demand = demandcount[1]
        #else:
        #    Demand = 'No Run'
        if len(mstcount) > 1:
            MSTs = mstcount[1]
        #    MST_packing = mstpack[1]
        #else:
        #    MSTs = 'No Run'
        #    MST_packing = 'No Run' 
        if len(fapcount) > 1:
            FAPs = fapcount[1]
        #    FAP_packing = fappack[1]
        #else:
        #    FAPs = 'No Run'
        #    FAP_packing = 'No Run'
        if len(fdhcount) > 1:
            FDHs = fdhcount[1]
        #    FDH_packing = fdhpack[1]
        #else:
        #    FDHs = 'No Run'
        #    FDH_packing = 'No Run'
    some_info["Demand"] = Demand
    some_info["MSTs"] = MSTs
    some_info["MST_packing"] = MST_packing
    some_info["FAPs"] = FAPs
    some_info["FAP_packing"] = FAP_packing
    some_info["FDHs"] = FDHs
    some_info["FDH_packing"] = FDH_packing
    
    
    # packtier = re.split(':?', tier_msg)
    # performtier = re.split("Not performing", tier_msg)
    # if len(packtier) == 1 and len(performtier) > 1:
        # tier_packing = 'No Run'
    # elif len(packtier) == 2:
        # tier_packing = packtier[1]
    # else:
        # tier_packing = 'No info'
    # some_info.append(tier_packing)
 
### Read in input_data.json ### 
with open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_2\input_data.json", 'r') as ff:
    x1= json.load(ff)

some_info = {}
for key0 in x1.keys():
    if re.search('candidate_graph',key0):
        cand_graph = x1.get(key0)
    elif re.search('input_design_graph', key0):
        cand_graph = x1.get(key0)
    # Input parameters here! #
    elif re.search('args', key0):
        args = x1.get(key0)
        for key in args.keys(): 
            if re.search('use_hub_balancing',key):
                hub_balance = args.get(key)
            elif re.search('cable_cost_mst',key):
                cable_cost_mst = args.get(key)
            elif re.search('max_drop_length_ar',key):
                drop_length_aer = args.get(key)
            elif re.search('max_drop_length_ug',key):
                drop_length_ug = args.get(key)
            elif re.search('reuse_factor',key):
                reuse_factor = args.get(key)
            elif re.search('min_fdh_to_use',key):
                min_fdh = args.get(key)
            elif re.search('prune_distribution_input_design_graph',key):
                prune_dist = args.get(key)
            elif re.search('run_fdh_packing',key):
                run_fdh = args.get(key)
            elif re.search('hub_installation_cost_mst',key):
                mst_hub_cost = args.get(key)
            elif re.search('fdh_max_solver_duration_1_hub_packing',key):
                fdh_max_1_hub = args.get(key)
            elif re.search('run_nap_packing',key):
                run_fap = args.get(key)
            elif re.search('run_distribution_solver',key):
                run_dist = args.get(key)
            elif re.search('treat_hubs_as',key):
                treat_hubs_as = args.get(key)
            elif re.search('max_candidate_connections_mst',key):
                max_candidate_connections_mst = args.get(key)
            elif re.search('mip_stop_gap_fdh',key):
                mip_fdh = args.get(key)
            elif re.search('mip_stop_gap_nap',key):
                mip_fap = args.get(key)
            elif re.search('mip_stop_gap_dist',key):
                mip_dist = args.get(key)
            elif re.search('mip_stop_gap_mst',key):
                mip_mst = args.get(key)
            elif re.search('discount_preference_value',key):
                discount_preference = args.get(key)
            elif re.search('access_cabling_algo',key):
                cabling_algo = args.get(key)
                
                
cg_links = cand_graph.get("links",'No info')
num_aer = num_ug = len_aer = len_ug = 0
for arc in cg_links:
    if arc['TYPE'] == 'UG':
        num_ug += 1
        len_ug += arc['LENGTH']
    elif arc['TYPE']== 'AER':
        num_aer += 1 
        len_aer += arc['LENGTH']
        
        
some_info["Length_UG"] = len_ug
some_info["Length_AER"] = len_aer
some_info["Arc_UG"] = num_ug
some_info["Arc_AER"] = num_aer


some_info["use_hub_balancing"] = hub_balance
some_info["cable_cost_mst"] = cable_cost_mst
some_info["max_drop_length_ar"] = drop_length_aer 
some_info["max_drop_length_ug"] = drop_length_ug
some_info["reuse_factor"] = reuse_factor
some_info["min_fdh_to_use"] = min_fdh
some_info["prune_distribution_input_design_graph"] = prune_dist
some_info["run_fdh_packing"] = run_fdh
some_info["hub_installation_cost_mst"] = mst_hub_cost
some_info["fdh_max_solver_duration_1_hub_packing"] = fdh_max_1_hub
some_info["run_nap_packing"] = run_fap
some_info["run_distribution_solver"] = run_dist
some_info["treat_hubs_as"] = treat_hubs_as
some_info["max_candidate_connections_mst"] = max_candidate_connections_mst
some_info["mip_stop_gap_fdh"] = mip_fdh
some_info["mip_stop_gap_nap"] = mip_fap
some_info["mip_stop_gap_dist"] = mip_dist
some_info["mip_stop_gap_mst"] = mip_mst
some_info["discount_preference_value"] = discount_preference
some_info["access_cabling_algo"] = cabling_algo




with open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\solution.json", 'r') as fs:
    x1 = json.load(fs)
























    
