### FOND CALIBRATION -- TIME SAVING ###


### Read in the data ###
#jul = read.csv('C:/Users/Chang Liu/Documents/Hut Merge/Annual Questions/Networks Data-Jul2015.csv',header =T)

months_b = c('Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb','Mar','Apr','May')
year_b = c(rep(2015,6), rep(2016,5))

folder_b = 'C:/Users/Chang Liu/Documents/Hut Merge/Annual Questions/'
csv_names = paste('Networks Data-', months_b,year_b,'.csv', sep = '')

#Cannot assign a character value as dataframe...#
entire_file_name = paste(folder_b,csv_names, sep = '')
# for (i in 1: length(months_b))
# {

#   months_b[i] = read.csv(entire_file_name[i], header = T)
# }

Jul = read.csv(entire_file_name[1], header = T)
Aug = read.csv(entire_file_name[2], header = T)
Sep = read.csv(entire_file_name[3], header = T)
Oct = read.csv(entire_file_name[4], header = T)
Nov = read.csv(entire_file_name[5], header = T)
Dec = read.csv(entire_file_name[6], header = T)
Jan = read.csv(entire_file_name[7], header = T)
Feb = read.csv(entire_file_name[8], header = T)
Mar = read.csv(entire_file_name[9], header = T)
Apr = read.csv(entire_file_name[10], header = T, skip = 6)
May = read.csv(entire_file_name[11], header = T, skip = 6)



entire_data = rbind(Jul,Aug,Sep,Oct,Nov,Dec,Jan,Feb,Mar,Apr,May)

# > names(entire_data)
#  [1] "Date"        "Client"      "Task"        "User"        "Start"       "End"         "Dur.hrs."
#  [8] "Rate"        "Category"    "Tags"        "Jira.ID"     "Description"
Date_year = as.Date(entire_data$Date, '%d/%m/%Y')
entire_data$Date = Date_year
entire_data$Month = format(Date_year, '%B')

# Unique date, sorted
Unique_date = sort(unique(Date_year))

# Sorted workday
library(timeDate)
Weekday = Unique_date[which(isWeekday(Unique_date))]
Weekend = Unique_date[which(!isWeekday(Unique_date))]

# Find anything related to solve
G_solve = unique(entire_data$Task)[85]
Solve_Data = entire_data[entire_data$Task == G_solve,]
#######################################################################
#######################################################################
################### CODE ABOVE IS NO LONGER REQUIRED TO RUN AGAIN #####


### Write the Solve related output
#write.csv(Solve_Data, "C:\\Users\\Chang Liu\\Documents\\Data Science\\Solve Time\\Chang's BAT QuickTS Data.csv")

### Read the data again ###
Solve_Data = read.csv("C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Chang's BAT QuickTS Data.csv", header = T)
### Read in REPLICON data ###
Time.Rep = read.csv("C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Chang's BAT Replicon Data.csv", header = T)
#names(Time.Rep)
# [1] "Timesheet.End.Date"    "User.Name"            
# [3] "Program.Name"          "Project.Name"         
# [5] "Task.Name..Full.Path." "Classification"       
# [7] "Total.Hrs"  
Pre_CRO = grepl('pre-CRO', Time.Rep$Project.Name)
Post_CRO = grepl('post-CRO', Time.Rep$Project.Name)
pre_post = rep(0, length(Time.Rep$User.Name))
pre_post[Pre_CRO] = 'pre-CRO'
pre_post[Post_CRO] = 'post-CRO'
Time.Rep$Version = pre_post
Hut.Data = Time.Rep[pre_post!=0,]


### Clean data. Group by hut name + version
#dim(Solve_Data)
Descri = as.character(tolower(Solve_Data$Description))
#unique(substr(Desc, start = 1, stop = 6))

### AUS data ###
AUS = which(substr(Descri, start = 1, stop = 3) =='aus')
AUS.Hut = sort(substr(Descri[AUS], start = 1, stop = 6))
Descri[AUS]

Version.AUS = c()
for (a1 in 1:length(AUS)) {
  
  if(grepl('v2',Descri[AUS[a1]]))
  {
    Version.AUS[a1] = 'pre-CRO v2'
  }else if(grepl('v3',Descri[AUS[a1]]))
  {
    Version.AUS[a1] = 'pre-CRO v3'
  }else if(grepl('v5',Descri[AUS[a1]]))
  {
    Version.AUS[a1] = 'pre-CRO v5'
  }else if(grepl('cro',Descri[AUS[a1]]) |grepl('reload',Descri[AUS[a1]]) )
  {
    Version.AUS[a1] = 'post-CRO v1'
  }else if (nchar(Descri[AUS[a1]])> 30 )
  {
    Version.AUS[a1] = 'NULL'
  }else{
    Version.AUS[a1] = 'pre-CRO v1'
  }
  
}
Solve_Data$Version = rep(0, dim(Solve_Data)[1])
Solve_Data$Version[AUS] = Version.AUS

### SDF data ###
SDF = which(substr(Descri, start = 1, stop = 3) =='sdf')
SDF.Hut = sort(substr(Descri[SDF], start = 1, stop = 6))
Descri[SDF]
Version.SDF = c()
for (a1 in 1:length(SDF)) {
  
  if(grepl('v2',Descri[SDF[a1]]) & !grepl('reload',Descri[SDF[a1]] ) &
     !grepl('cro',Descri[SDF[a1]] ))
  {
    Version.SDF[a1] = 'pre-CRO v2'
  }else if((grepl('cro',Descri[SDF[a1]]) |grepl('reload',Descri[SDF[a1]])) &
           grepl('v2',Descri[SDF[a1]]))
  {
    Version.SDF[a1] = 'post-CRO v2'
  }else if ((grepl('cro',Descri[SDF[a1]]) |grepl('reload',Descri[SDF[a1]])) &
            !grepl('v2',Descri[SDF[a1]]) )
  {
    Version.SDF[a1] = 'post-CRO v1'
  }else{
    Version.SDF[a1] = 'pre-CRO v1'
  }
  
}
check.SDF = cbind(Version.SDF,Descri[SDF])
Solve_Data$Version[SDF] = Version.SDF
### PDX data ###
PDX = which(substr(Descri, start = 1, stop = 3) =='pdx')

unique(PDX.Hut)
#PDX.Hut.Unique = paste('pdx',c(seq(106,116), 119,121,122,123,129), sep = '')
#PDX.Hut.No = c(seq(106,116), 119,121,122,123,129)

Descri[PDX]
Spaced_Entry = which(substr(Descri[PDX], start = 4, stop = 4) == ' ')
New.Descri = Descri
New.Descri[PDX][Spaced_Entry] = 'pdx121'
Incorrect.Hut = which(grepl('pdx001', Descri))
New.Descri[Incorrect.Hut] = 'pdx106'

PDX.Hut = sort(substr(New.Descri[PDX], start = 1, stop = 6))
Version.PDX = c()
for (a1 in 1:length(PDX)) {
  
  if(grepl('v2',Descri[PDX[a1]]) & !grepl('reload',Descri[PDX[a1]] ) &
     !grepl('cro',Descri[PDX[a1]] ))
  {
    Version.PDX[a1] = 'pre-CRO v2'
  }else if((grepl('cro',Descri[PDX[a1]]) |grepl('reload',Descri[PDX[a1]])) &
           grepl('v2',Descri[PDX[a1]]))
  {
    Version.PDX[a1] = 'post-CRO v2'
  }else if ((grepl('cro',Descri[PDX[a1]]) |grepl('reload',Descri[PDX[a1]])) &
            !grepl('v2',Descri[PDX[a1]]) & !grepl('cross',Descri[PDX[a1]]) )
  {
    Version.PDX[a1] = 'post-CRO v1'
  }else if (grepl('v3',Descri[PDX[a1]]))
  {
    Version.PDX[a1] = 'pre-CRO v3'
  }
  
  else{
    Version.PDX[a1] = 'pre-CRO v1'
  }
  
}
check.PDX = cbind(Version.PDX,Descri[PDX])
Solve_Data$Version[PDX] = Version.PDX


#Solve_Data$Description = 
#June = which(Solve_Data$User == 'June Yau')
#Solve_Data[June,]
#which(grepl('pdx001', tolower(Solve_Data$Description)))

#unique(tolower(substr(Solve_Data$Description, start = 1, stop = 6)))

### MCI data ###
MCI = which(substr(Descri, start = 1, stop = 3) =='mci')
MCI.Hut = sort(substr(Descri[MCI], start = 1, stop = 6))
New.Descri[MCI]
Version.MCI = c()
for (a1 in 1:length(MCI)) {
  
  if(grepl('v2',Descri[MCI[a1]]) )
  {
    Version.MCI[a1] = 'pre-CRO v1'
  }else if (grepl('ug',Descri[MCI[a1]]))
  {
    Version.MCI[a1] = 'pre-CRO v1'
  }
  
  else{
    Version.MCI[a1] = 0
  }
  
}
check.MCI = cbind(Version.MCI,Descri[MCI])
Solve_Data$Version[MCI] = Version.MCI


### JAX data ###
JAX = which(substr(Descri, start = 1, stop = 3) =='jax')
JAX.Hut = sort(substr(Descri[JAX], start = 1, stop = 6))
New.Descri[JAX]
Version.JAX = rep('pre-CRO v1', length(JAX))  
Solve_Data$Version[JAX] = Version.JAX  
Solve_Data$Hut = toupper(substr(New.Descri, start = 1, stop = 6))

Solve.Data.QuickT = Solve_Data[Solve_Data$Version!=0,]
Solve.Data.QuickT = Solve.Data.QuickT[,c(2,5,8,13,14,15,16)]

# Lewis booked the wrong thing, update#
tocorrect = which(Solve.Data.QuickT$User == 'Lewis Kerin' & Solve.Data.QuickT$Hut =='AUS107')
Solve.Data.QuickT$Version[tocorrect] = 'pre-CRO v3'
### Save QuickTime solve data into a csv ###
#write.csv(Solve.Data.QuickT, 'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\QuickTS Useful Solve.csv' )

##################################################################
##################################################################
##################################################################

#Solve.Data.QuickT = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\QuickTS Useful Solve.csv', header = T)
### Now, deal with Replicon data ###
################################################################
#THIS IS THE DATA FROM REPLICON!#
################################################################
Solving = grepl('Solving', Hut.Data$Task.Name..Full.Path.)
Solve.Data.Replicon = Hut.Data[Solving,]
Hut_Name = substr(Solve.Data.Replicon$Task.Name..Full.Path., start = 1, stop = 6)
unique(Hut_Name)
Solve.Data.Replicon$HUT = Hut_Name
################################################################
Daterep = as.Date(Solve.Data.Replicon$Timesheet.End.Date, '%d/%m/%Y')
Versionrep = c()
Username = as.character(Solve.Data.Replicon$User.Name)
userrep = c()
for (v1 in 1: length(Daterep))
{
  if (Solve.Data.Replicon$Version[v1] == 'pre-CRO')
  {
    Versionrep[v1] = 'pre-CRO v1'
  }else{
    Versionrep[v1] = 'post-CRO v1'
  }
  userlist = strsplit(Username[v1],split = ', ')
  userrep[v1] = paste( userlist[[1]][2], userlist[[1]][1], sep = ' ')
}
SDR = data.frame(Date = Daterep, User = userrep,
                 Dur.hrs.=Solve.Data.Replicon$Total.Hrs,
                 Description = Solve.Data.Replicon$Task.Name..Full.Path.,
                 Month = format(Daterep, '%B'),
                 Version= Versionrep,
                 Hut = Solve.Data.Replicon$HUT  )



### FIND info with respect to each design ###
SDQ = Solve.Data.QuickT
# wrong_version_loc = which(SDQ$User == 'Lewis Kerin' & SDQ$Hut == 'AUS107')
# SDQ$Version[wrong_version_loc] = 'pre-CRO v3'
# #more incorrect --> AUS115, AUS119, and MCI155 #
# SDQ$Date[which(SDQ$Hut == 'AUS115')]

SDQ_Hut = unique(sort(SDQ$Hut))
modloc = c()
SDQ$Version = as.character(SDQ$Version)
for (sd1 in 1: length(SDQ_Hut))
{
 Hut_Ver = unique(as.character(SDQ$Version[SDQ$Hut == SDQ_Hut[sd1]])) 
 for (v in 1: length(Hut_Ver))
 {
   Vloc = which(SDQ$Hut == SDQ_Hut[sd1] & SDQ$Version == Hut_Ver[v])
   Vdate = sort(as.Date(SDQ$Date[Vloc]))
   probloc = which(Vdate[-1] - Vdate[-length(Vdate)] >=10)
   Cutoff_date = Vdate[probloc + 1]
   
   if(length(Cutoff_date) > 0)
   {
     for(cc1 in 1: length(Cutoff_date))
     {
       cd = Cutoff_date[cc1]
       Changeloc = which(SDQ$Hut == SDQ_Hut[sd1] & SDQ$Version == Hut_Ver[v] & as.Date(SDQ$Date) >= cd & as.Date(SDQ$Date) <= cd+25)
       if ( (v+1) <= length(Hut_Ver))
       {
         Vloc_next =  which(SDQ$Hut == SDQ_Hut[sd1] & SDQ$Version == Hut_Ver[v+1])
         Vdate_next = sort(as.Date(SDQ$Date[Vloc_next]))[1]
         
         if( Vdate_next - cd <= 5)
         {
           SDQ$Version[Changeloc] = Hut_Ver[v+1]
         }else{
           SDQ$Version[Changeloc] = paste(substr(Hut_Ver[v], start= 1, stop = nchar(Hut_Ver[v])-1), 
                                          as.numeric(substr(Hut_Ver[v], start= nchar(Hut_Ver[v]), stop = nchar(Hut_Ver[v]))) + 1, sep = '')
         }
       }else{
         if(Hut_Ver[v] == 'pre-CRO v3' & substr(SDQ_Hut[sd1], start = 1, stop = 3) == 'PDX')
         {
           SDQ$Version[Changeloc] = 'post-CRO v1'
         }else{
           SDQ$Version[Changeloc] = paste(substr(Hut_Ver[v], start= 1, stop = nchar(Hut_Ver[v])-1), 
                                          as.numeric(substr(Hut_Ver[v], start= nchar(Hut_Ver[v]), stop = nchar(Hut_Ver[v]))) + 1, sep = '')
         }
         
       }
       modloc = c(modloc,Changeloc) 
     }
     # Changeloc = which(SDQ$Hut == SDQ_Hut[sd1] & SDQ$Version == Hut_Ver[v] & as.Date(SDQ$Date) >= Cutoff_date)
     # if ( (v + 1) <= length(Hut_Ver))
     # {
     #   Vloc_next =  which(SDQ$Hut == SDQ_Hut[sd1] & SDQ$Version == Hut_Ver[v+1])
     #   Vdate_next = sort(as.Date(SDQ$Date[Vloc_next]))[1]
     #   
     #   if( Vdate_next - Cutoff_date <= 5)
     #   {
     #     SDQ$Version[Changeloc] = Hut_Ver[v+1]
     #   }else{
     #     SDQ$Version[Changeloc] = paste(substr(Hut_Ver[v], start= 1, stop = nchar(Hut_Ver[v])-1), 
     #     as.numeric(substr(Hut_Ver[v], start= nchar(Hut_Ver[v]), stop = nchar(Hut_Ver[v]))) + 1, sep = '')
     #   }
     # }
     
   }
   
      
 }
}

Date = c(as.character(SDQ$Date),as.character(Daterep))
User = c(as.character(SDQ$User),as.character(userrep))
Hours = c(as.numeric(SDQ$Dur.hrs.),as.numeric(as.character(Solve.Data.Replicon$Total.Hrs)))
Description = c(as.character(SDQ$Description),as.character(Solve.Data.Replicon$Task.Name..Full.Path.))
Version = c(as.character(SDQ$Version),as.character(Versionrep))
HutName = c(as.character(SDQ$Hut),as.character(Solve.Data.Replicon$HUT))

SDTT = data.frame(Date = Date, User = User,
                  Hours=Hours,
                  Description = Description,
                  Version= Version,
                  Hut = HutName  )
SDTT = SDTT[ - which(SDTT$Version == 'NULL'),]
SDTT$Version[which(SDTT$Version == 'pre-CRO v5')] = 'pre-CRO v3'
SDTT = SDTT[ - which(SDTT$User == 'Mi Do'),]
Design = paste(SDTT$Hut, SDTT$Version, sep = ' ')
GF_Designs = sort(unique(Design))
Booked_Time = c()
Versions = c()
Huts = c()
Start_Time = c()
Users = c()
Users.group = c()
for (a1 in 1: length(GF_Designs))
{
  Huts[a1] = substr(GF_Designs[a1],start = 1, stop = 6)
  Versions[a1] = substr(GF_Designs[a1], start = 8, stop = nchar(GF_Designs[a1]))
  tmp_loc = which(SDTT$Hut == Huts[a1] &
                    SDTT$Version == Versions[a1] )
  Booked_Time[a1] = sum(as.numeric(SDTT$Hours[tmp_loc]))
  #Start_Time[a1] = as.character(SDTT$Date[tmp_loc[1]])
  Start_Time[a1] = paste(sort(unique(as.character(SDTT$Date[tmp_loc]))), collapse = ',')
  Mult_User = sort(unique(as.character(SDTT$User[tmp_loc])))
  time_most = c()
  for ( m1 in 1: length(Mult_User))
  {
    hrs = SDTT$Hours[tmp_loc]
    time_most[m1] = sum(hrs[SDTT$User[tmp_loc] == Mult_User[m1]])
  }
  Users[a1] = Mult_User[which(time_most == max(time_most))[1]]
  Users.group[a1] = paste(sort(unique(as.character(SDTT$User[tmp_loc]))),sep = '',collapse = ',')
}

Solve.DF = data.frame(Start_Time,Huts,Versions,Booked_Time,Users,Users.group)


### ADD back in the hut info from GDB files ###
AUSIN = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\AUS_HUT_INFO.csv', header = T)
SDFIN = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\SDF_HUT_INFO.csv', header = T)
PDXIN = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\PDX_HUT_INFO.csv', header = T)
MCIIN = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\MCI_HUT_INFO.csv', header = T)
JAXIN = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\JAX_HUT_INFO.csv', header = T)
Cityin = rbind(AUSIN,SDFIN,PDXIN,MCIIN,JAXIN)
Solve.DF$Num_Addie = Cityin[match(Solve.DF$Huts,Cityin$HUTID),'NUMADD']
Solve.DF$AER_MILEAGE = Cityin[match(Solve.DF$Huts,Cityin$HUTID),'AER_MILEAGE']
Solve.DF$UG_MILEAGE = Cityin[match(Solve.DF$Huts,Cityin$HUTID),'UG_MILEAGE']
Solve.DF$Month = format(as.Date(Start_Time),'%B')
Solve.DF$Market = substr(Huts,start = 1, stop = 3)
df = Solve.DF[order(as.Date(Solve.DF$Start_Time)),]
row.names(df) = 1:dim(df)[1]
#write.csv(df, 'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Solve_Non_Server_Full.csv' )
#df = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Solve_Non_Server_Full.csv', header =T)



### Add data. The columns we need are: 1) Number of addresses
### 2) AERIAL mileage 3) UG mileage 4) Design Version
### 5) FOND version 6) MIP_STOP_GAP; Solve Time

library(sp)
library(rgdal)
library(rgeos)


# AUS_CITY_PATH = readOGR("C:\\Users\\chang\\Documents\\City Data\\AUS_20160729_FULL_GID.gdb",
#         layer = 'PATH',stringsAsFactors = F)
# AUS_CITY_ADDIE = readOGR("C:\\Users\\chang\\Documents\\City Data\\AUS_20160729_FULL_GID.gdb",
#         layer = 'ADDRESS',stringsAsFactors = F)
# AUS_HUTAREA = readOGR("C:\\Users\\chang\\Documents\\City Data\\AUS_20160729_FULL_GID.gdb",
#         layer = 'HUTAREA',stringsAsFactors = F)
# 
# SDF_CITY_PATH = readOGR("C:\\Users\\chang\\Documents\\City Data\\SDF_20160715_FULL_GID.gdb",
#         layer = 'PATH',stringsAsFactors = F)
# SDF_CITY_ADDIE = readOGR("C:\\Users\\chang\\Documents\\City Data\\SDF_20160715_FULL_GID.gdb",
#         layer = 'ADDRESS',stringsAsFactors = F)
# SDF_HUTAREA = readOGR("C:\\Users\\chang\\Documents\\City Data\\SDF_20160715_FULL_GID.gdb",
#         layer = 'HUTAREA',stringsAsFactors = F)
# 
# PDX_CITY_PATH = readOGR("C:\\Users\\chang\\Documents\\City Data\\PDX_20160708_FULL_GID.gdb",
#         layer = 'PATH',stringsAsFactors = F)
# PDX_CITY_ADDIE = readOGR("C:\\Users\\chang\\Documents\\City Data\\PDX_20160708_FULL_GID.gdb",
#         layer = 'ADDRESS',stringsAsFactors = F)
# PDX_HUTAREA = readOGR("C:\\Users\\chang\\Documents\\City Data\\PDX_20160708_FULL_GID.gdb",
#         layer = 'HUTAREA',stringsAsFactors = F)
# 
# MCI_CITY_PATH = readOGR("C:\\Users\\chang\\Documents\\City Data\\MCI_20160527_FULL_GID.gdb",
#         layer = 'PATH',stringsAsFactors = F)
# MCI_CITY_ADDIE = readOGR("C:\\Users\\chang\\Documents\\City Data\\MCI_20160527_FULL_GID.gdb",
#         layer = 'ADDRESS',stringsAsFactors = F)
# MCI_HUTAREA = readOGR("C:\\Users\\chang\\Documents\\City Data\\MCI_20160527_FULL_GID.gdb",
#         layer = 'HUTAREA',stringsAsFactors = F)
# 
# JAX_CITY_PATH = readOGR("C:\\Users\\chang\\Documents\\City Data\\JAX_20160722_FULL_GID.gdb",
#         layer = 'PATH',stringsAsFactors = F)
# JAX_CITY_ADDIE = readOGR("C:\\Users\\chang\\Documents\\City Data\\JAX_20160722_FULL_GID.gdb",
#         layer = 'ADDRESS',stringsAsFactors = F)
# JAX_HUTAREA = readOGR("C:\\Users\\chang\\Documents\\City Data\\JAX_20160722_FULL_GID.gdb",
#         layer = 'HUTAREA',stringsAsFactors = F)

# AUS.HUTID = unique(AUS_HUTAREA@data$HUTID)
# names(AUS_CITY_PATH@data)


### FUNCTION to find addresses and cable length ###
Get_Hut_Data = function(some_path, some_addie, some_gon)
{
  HUTID = unique(some_gon@data$HUTID)
  # feet to mile ratio #
  ratio = 0.000189394
  Aer_Mile =  Ug_Mile = c()
  Num_Add = c()
  for (h1 in 1: length(HUTID))
  {
    Path_In_loc = as.vector(gContains(some_gon[h1,],some_path, byid = T))
    Addie_In_loc = as.vector(gContains(some_gon[h1,],some_addie, byid = T))
    pathin = some_path[Path_In_loc,]
    addiein = some_addie[Addie_In_loc,]
    Num_Add[h1] = dim(addiein)[1]
    Aer = pathin[pathin@data$Placement == 'Aerial',]
    Ug = pathin[pathin@data$Placement == 'Underground',]
    
    Aer_Mile[h1] = sum(as.numeric(Aer@data$CalculatedLength)) * ratio
    Ug_Mile[h1] = sum(as.numeric(Ug@data$CalculatedLength)) * ratio 
  }
  Result = data.frame(HUTID = HUTID, NUMADD = Num_Add,
                      AER_MILEAGE = Aer_Mile, UG_MILEAGE = Ug_Mile)
  Result
}

#write.csv(Result,'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\AUS_HUT_INFO.csv')


sdf_data = Get_Hut_Data(some_path = SDF_CITY_PATH,some_addie = SDF_CITY_ADDIE,
                        some_gon = SDF_HUTAREA)
write.csv(sdf_data,'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\SDF_HUT_INFO.csv')

################################
pdx_data = Get_Hut_Data(some_path = PDX_CITY_PATH,some_addie = PDX_CITY_ADDIE,
                        some_gon = PDX_HUTAREA)
write.csv(pdx_data,'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\PDX_HUT_INFO.csv')

################################
mci_data = Get_Hut_Data(some_path = MCI_CITY_PATH,some_addie = MCI_CITY_ADDIE,
                        some_gon = MCI_HUTAREA)
write.csv(mci_data,'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\MCI_HUT_INFO.csv')

################################
jax_data = Get_Hut_Data(some_path = JAX_CITY_PATH,some_addie = JAX_CITY_ADDIE,
                        some_gon = JAX_HUTAREA)
write.csv(jax_data,'C:\\Users\\chang\\Documents\\DS work\\Solve Time\\JAX_HUT_INFO.csv')


#############################################################
#############################################################
#############################################################




### NOW the ANALYSIS! ###
#names(Solve.DF)
#order(as.Date(Solve.DF$Start_Time))
df = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Solve_Non_Server_Full.csv',header = T)
#Solve.DF$Market = as.factor(Solve.DF$Market)
df = df[-which(df$Booked_Time <8),]
df$Users = as.character(df$Users)
df$Users[df$Users == 'Alex Brazel'] = 'Alexander Brazel'
df$Users = factor(df$Users)


# Take a look at the matrix plot #
par('mar')
#par(mar = c(5.1,4.1,4.1,2.1))
#par(mar = c(1,1,1,1))
pairs(Booked_Time~Num_Addie + AER_MILEAGE + UG_MILEAGE + Market + Users + Huts + Month , data = df)
pairs(Booked_Time~ Num_Addie + AER_MILEAGE + UG_MILEAGE +factor(Month) + factor(Users), data = df)
# Rstudio sucks, as it finds the figure margins too large!! #
# Can run it using R console #
pairs20x(log(Booked_Time)~ Num_Addie + AER_MILEAGE + UG_MILEAGE +factor(Month)+ factor(Market) + factor(Users) +factor(Huts) , data = df)
pairs20x(log(Booked_Time)~ factor(Market) + factor(Users) +factor(Huts) , data = df)





lm1 = lm(log(Booked_Time)~ Num_Addie + AER_MILEAGE + UG_MILEAGE +Month+ Market + Users +Huts + Versions , data = df)
summary(lm1)
lm2 = lm(log(Booked_Time)~ Num_Addie + AER_MILEAGE + UG_MILEAGE +Month+ Market + Users + Versions, data = df)
summary(lm2)

unique(df$Month)
levels(df$Month) = c('August','September','October','November','December','January','February',
'March', 'April', 'May', 'June', 'July')
# Find User = ChangLiu, set as baseline #
UserVec = as.character(levels(df$Users))
#levels(df$Users) = c(UserVec[5], UserVec[-5])
relevel(df$Users, ref = c(UserVec[3], UserVec[-3]))
levels(df$Market) = c('JAX','AUS', 'SDF','PDX','MCI')
VerVec = as.character(levels(df$Versions))

### So far lm3 is the best, with highest multiple and adjusted R squared###
lm3 = lm(log(Booked_Time)~ AER_MILEAGE + UG_MILEAGE +Month+ Market + factor(Users,
levels =c(UserVec[3], UserVec[-3])) + factor(Versions, levels = c(VerVec[3],VerVec[-3])), data = df)
summary(lm3)

lm4 = lm(log(Booked_Time)~ AER_MILEAGE + UG_MILEAGE +Market + factor(Users,
          levels =c(UserVec[3], UserVec[-3])) + factor(Versions, levels = c(VerVec[3],VerVec[-3])), data = df)
summary(lm4)

lm5 = lm(log(Booked_Time)~ AER_MILEAGE + UG_MILEAGE +Market + factor(Users,
          levels =c(UserVec[3], UserVec[-3])) , data = df)
summary(lm5)

glm1 = glm(Booked_Time ~ Huts + Versions + Num_Addie + AER_MILEAGE + UG_MILEAGE + Month + factor(Market), data = Solve.DF)
glm1

df$Users_g = as.character(df$Users)
df$Users_g[grep(',', df$Users)] = 'Multiple'
df$Users_g = as.factor(df$Users_g)

levels(df$Users_g)

lm4 = lm(log(Booked_Time)~ AER_MILEAGE + UG_MILEAGE +Month+ Market + Users_g, data = df)
summary(lm4)

#########################################################################################################
#########################################################################################################
#########################################################################################################
#########################################################################################################
#########################################################################################################


### READ IN the log data from the server!!! ###
#server_old = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\mycsvfile.csv',header = T)
#server_old = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Waldo.csv',header = T)
#server_new = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\mycsvfile_fromKingkong1.csv',header = T)
#server_new = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Kingkong1.csv',header = T)

server_waldo = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Waldo_With_Input_Parameters.csv',header = T)
server_kingkong1 = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\KingKong1_With_Input_Parameters.csv',header = T)
server = as.data.frame(rbind(server_waldo,server_kingkong1))


server = server[server$solve_finish == 'True' & server$run_time!= 'No info' & server$last_modified!= 'No info',]
server = server[server$user!='jenkins'& server$Demand != 'No Run',]

# Daniel ran two solves, with MST_packing >65. We remove them.#
server = server[which(as.numeric(paste(server$MST_packing)) <=8),]


server_date = as.character(substr(server$submit_time, start = 1, stop = 10))
server_date = as.Date(server_date)
server_user = as.character(server$user)
server$date = server_date
run_time = as.numeric(paste(server$run_time))

server$runtime = run_time
server$version = as.factor(as.character(server$version))
server$user = as.factor(as.character(server$user))
server$Demand = as.numeric(paste(server$Demand))


server$MSTs = as.numeric(paste(server$MSTs))
server$MST_packing = as.character(paste(server$MST_packing))
server$MST_packing[1] = 40/7
server$MST_packing[server$MST_packing =='No Run'] = 0
server$MST_packing = as.numeric(server$MST_packing)
server$FAPs = as.character(paste(server$FAPs))
server$FAPs[server$FAPs =='No Run'] = 0
server$FAPs = as.numeric(server$FAPs)
server$FAP_packing = as.character(paste(server$FAP_packing))
server$FAP_packing[server$FAP_packing =='No Run'] = 0
server$FAP_packing = as.numeric(server$FAP_packing)
server$FDHs = as.character(paste(server$FDHs))
server$FDHs[server$FDHs =='No Run'] = 0
server$FDHs = as.numeric(server$FDHs)
server$FDH_packing = as.character(paste(server$FDH_packing))
server$FDH_packing[server$FDH_packing =='No Run'] = 0
server$FDH_packing = as.numeric(server$FDH_packing)
server$Dist = as.factor(as.character(server$Dist))
server$COF = as.factor(as.character(server$COF))
server$Arc_UG = as.numeric(paste(server$Arc_UG))
server$Arc_AER = as.numeric(paste(server$Arc_AER))
server$Arcs = server$Arc_UG + server$Arc_AER
server$Length = server$Length_AER + server$Length_UG
#new parameters #
server$use_hub_balancing = as.factor(as.character(server$use_hub_balancing))
server$cable_cost_mst = as.numeric(paste(server$cable_cost_mst))
server$max_drop_length_ar = as.numeric(paste(server$max_drop_length_ar))
server$reuse_factor = as.numeric(paste(server$reuse_factor))
server$min_fdh_to_use = as.numeric(paste(server$min_fdh_to_use))
server$prune_distribution_input_design_graph = as.factor(as.character(server$prune_distribution_input_design_graph))
server$max_drop_length_ug = as.numeric(paste(server$max_drop_length_ug))
server$run_fdh_packing = as.factor(as.character(server$run_fdh_packing))
server$hub_installation_cost_mst = as.numeric(paste(server$hub_installation_cost_mst))
#server$fdh_max_solver_duration_1_hub_packing = as.numeric(paste(server$fdh_max_solver_duration_1_hub_packing))
server$run_nap_packing = as.factor(as.character(server$run_nap_packing))
server$run_distribution_solver = as.factor(as.character(server$run_distribution_solver))
server$treat_hubs_as = as.factor(as.character(server$treat_hubs_as))
#server$max_candidate_connections_mst = as.numeric(paste(server$max_candidate_connections_mst))
server$mip_stop_gap_fdh = as.numeric(paste(server$mip_stop_gap_fdh))
server$mip_stop_gap_nap = as.numeric(paste(server$mip_stop_gap_nap))
server$mip_stop_gap_dist = as.numeric(paste(server$mip_stop_gap_dist))
server$mip_stop_gap_mst = as.numeric(paste(server$mip_stop_gap_mst))
server$discount_preference_value = as.numeric(paste(server$discount_preference_value))
server$access_cabling_algo = as.factor(as.character(server$access_cabling_algo))


# plot of Runtime VS Demand #
plot(sort(as.numeric(server$Demand)), server$runtime[order(as.numeric(server$Demand))])


# > names(server)
# [1] "jobid"         "solve_finish"  "user"         
# [4] "submit_time"   "version"       "last_modified"
# [7] "run_time"      "Demand"        "MSTs"         
# [10] "MST_packing"   "FAPs"          "FAP_packing"  
# [13] "FDHs"          "FDH_packing"   "Dist"         
# [16] "COF"           "date" 
lm1 = lm(runtime ~ version + Demand + MSTs + MST_packing +
         FAPs + FAP_packing + FDHs + FDH_packing +
         Dist + COF, data = server)
summary(lm1)

library(s20x)
pairs20x(runtime ~ version + Demand + MSTs + MST_packing +
           FAPs + FAP_packing + FDHs + FDH_packing +
           Dist + COF, data = server)



### MST solve data only ###
server_mst = server[which(server$FAPs ==0 & server$FDHs ==0 & server$run_nap_packing == 'False'),]

# Remove 727, 1538 and 1841
# Second round 
loc = which(row.names(server_mst) == 727 | row.names(server_mst) == 1538 |
            row.names(server_mst) == 1841)
loc = which(row.names(server_mst) == 727| row.names(server_mst) == 1538|
row.names(server_mst) == 1841|row.names(server_mst) == 462|row.names(server_mst) == 1507|
row.names(server_mst) == 1870|row.names(server_mst) == 397|row.names(server_mst) == 3082|row.names(server_mst) ==3451 )
server_mst_no_outlier = server_mst[-loc,]

pairs20x(log(runtime)~Demand *MSTs*MST_packing * Arc_AER * Length_AER * Arc_UG * Length_UG, data = server_mst)

lm_mst = lm(log(runtime)~Demand *MST_packing * Arc_AER * Length_AER * Arc_UG * Length_UG, data = server_mst)
### Demand is highly correlated with Arcs, so let us try dropping the demand term ###
lm_mst_nd = lm(log(runtime)~MST_packing * Arc_AER * Length_AER * Arc_UG * Length_UG, data = server_mst)
summary(lm_mst_nd)
#64% Adjusted R-squared#

lm_mst = lm(log(runtime)~Demand * MSTs * MST_packing * Arc_AER * Length_AER * Arc_UG * Length_UG *
              max_drop_length_ug * max_drop_length_ar *
              reuse_factor * hub_installation_cost_mst * mip_stop_gap_mst, data = server_mst)
summary(lm_mst)


lmm1 = lm_mst
dropterm(lmm1, test = 'F')


lmm1 = update(lmm1, .~. -Arc_AER:Arc_UG:Length_UG  )
summary(lmm1)$adj.r.squared
pval = summary(lmm1)$coefficients[,4]
pval[which(pval > 0.1)]

summary(lmm1)$call

lm_mst_fin = lm(formula = log(runtime) ~ Demand + MST_packing + Arc_AER + 
    Length_AER + Arc_UG + Length_UG + Demand:MST_packing + Demand:Arc_AER + 
    MST_packing:Arc_AER + Demand:Length_AER + MST_packing:Length_AER + 
    Arc_AER:Length_AER + Demand:Arc_UG + MST_packing:Arc_UG + 
    Arc_AER:Arc_UG + Length_AER:Arc_UG + Demand:Length_UG + Arc_AER:Length_UG + 
    Length_AER:Length_UG + Arc_UG:Length_UG + Demand:MST_packing:Arc_AER + 
    Demand:MST_packing:Length_AER + Demand:Arc_AER:Length_AER + 
    MST_packing:Arc_AER:Length_AER + Demand:MST_packing:Arc_UG + 
    Demand:Arc_AER:Arc_UG + MST_packing:Arc_AER:Arc_UG + Demand:Length_AER:Arc_UG + 
    MST_packing:Length_AER:Arc_UG + Arc_AER:Length_AER:Arc_UG + 
    Demand:MST_packing:Length_UG + Demand:Arc_AER:Length_UG + 
    MST_packing:Arc_AER:Length_UG + Demand:Length_AER:Length_UG + 
    MST_packing:Length_AER:Length_UG + Arc_AER:Length_AER:Length_UG + 
    MST_packing:Arc_UG:Length_UG + Length_AER:Arc_UG:Length_UG + 
    Demand:MST_packing:Arc_AER:Length_AER + Demand:MST_packing:Arc_AER:Arc_UG + 
    Demand:MST_packing:Length_AER:Arc_UG + Demand:Arc_AER:Length_AER:Arc_UG + 
    MST_packing:Arc_AER:Length_AER:Arc_UG + Demand:MST_packing:Arc_AER:Length_UG + 
    Demand:MST_packing:Length_AER:Length_UG + Demand:Arc_AER:Length_AER:Length_UG + 
    MST_packing:Arc_AER:Length_AER:Length_UG + Demand:MST_packing:Arc_UG:Length_UG + 
    Demand:Arc_AER:Arc_UG:Length_UG + Demand:Length_AER:Arc_UG:Length_UG + 
    MST_packing:Length_AER:Arc_UG:Length_UG + Demand:MST_packing:Arc_AER:Length_AER:Arc_UG + 
    Demand:MST_packing:Arc_AER:Length_AER:Length_UG + Demand:Arc_AER:Length_AER:Arc_UG:Length_UG + 
    Demand:MST_packing:Arc_AER:Length_AER:Arc_UG:Length_UG -Length_AER - Length_UG, data = server_mst_no_outlier)

summary(lm_mst_fin)

par(mfrow = c(2,2))
plot(lm_mst_fin)

exp(predict(lm_mst_fin, new = server_mst[16,], interval = 'prediction'))

### Predictions ###
oneval  = server_mst[16,]
df1 = data.frame(Demand = oneval$Demand, MSTs = oneval$MSTs,Arc_AER = oneval$Arc_AER,
                 Length_AER = oneval$Length_AER, Arc_UG = oneval$Arc_UG,
                 Length_UG = oneval$Length_UG)
inte = 0.01 * -50:50
mst_d = oneval$MST_packing + inte
datam = as.matrix(rep(oneval, 100),nrow = 100, ncol = dim(oneval)[2])
Pred = predict(lm_mst_fin,new = data.frame(df1, MST_packing = mst_d), interval = 'prediction')

### Something went wrong. Runtim drops when MST_packing increases?!! ###
### Turns out, there is a strong relationship between MSTs & Arcs ###
mst_lm = lm(MSTs~Arc_AER * Arc_UG * Length_AER * Length_UG, data = server)
summary(mst_lm)
mst1 = update(mst1, .~. -Arc_UG)
summary(mst1)
fap_lm = lm(FAPs~ Arc_AER * Arc_UG * Length_AER * Length_UG, data = server_fap)
summary(fap_lm)
fdh_lm = lm(FDHs~MSTs * FAPs * Arc_AER * Arc_UG * Length_AER * Length_UG, data = server_fdh)
summary(fdh_lm)

mst_fin = lm(formula = MSTs ~ Arc_AER + Length_AER + Length_UG + Arc_AER:Arc_UG + 
               Arc_AER:Length_AER + Arc_AER:Length_UG + Length_AER:Length_UG + 
               Arc_AER:Arc_UG:Length_AER + Arc_AER:Length_AER:Length_UG + 
               Arc_AER:Arc_UG:Length_AER:Length_UG, data = server)
summary(mst_fin)

d_lm = lm(Demand~Arc_AER * Arc_UG , data = server)
d1 = d_lm
d1 = update(d1, .~. -Arc_AER:Arc_UG:Length_AER:Length_UG )
summary(d1)
demand_fin = lm(formula = Demand ~ Arc_AER + Arc_UG + Length_AER + Length_UG + 
                  Arc_AER:Arc_UG + Arc_AER:Length_AER + Arc_AER:Length_UG + 
                  Length_AER:Length_UG + Arc_AER:Arc_UG:Length_UG + Arc_UG:Length_AER:Length_UG, 
                data = server)
summary(demand_fin)
### Or, we can just simply ignore Lengths variables, and get a 0.9414 R-squared. ###



# AIC 1858.8, lowest, but might cause over fitting, because of the strong
# correlation between Demand and MSTs.#
server_mst_no_outlier$COF = as.factor(as.character(server_mst_no_outlier$COF))
glm_mst_6 = glm(log(runtime)~ Demand + MSTs + MST_packing +
                  Demand:MSTs:MST_packing + Arc_AER   +
                  Arc_UG * Length_UG + Demand:Arc_AER:Length_AER +
                  Demand:Arc_UG:Length_UG,  data = server_mst_no_outlier)
summary(glm_mst_6)

### FAP solve data only ###
server_fap = server[which(server$FAPs!= 0 & server$FDHs == 0),]

# Get rid of an outlier, with FAPs > 1200
server_fap = server_fap[server_fap$FAPs < 1200,]
pairs20x(log(runtime)~Demand + MSTs + MST_packing + version + FAPs + FAP_packing + Arc_AER +
Length_AER + Arc_UG + Length_UG + COF, data = server_fap)


lm_fap_comp = lm(log(runtime)~Demand *MSTs*MST_packing*FAPs*FAP_packing * Arc_AER * Length_AER * Arc_UG * Length_UG +
  COF, data = server_fap)
summary(lm_fap_comp)
#lm_fap = lm(log(runtime)~Demand *MST_packing*FAPs*FAP_packing + COF + version + Arc_AER * Length_AER + Arc_UG * Length_UG, data = server_fap)

# Some outlier, 3378, 1234
tr = which(rownames(server_fap) == 3378 | rownames(server_fap) == 1234)
server_fap_no_outlier = server_fap[-tr,]


lm_fap = lm(log(runtime)~Demand *MST_packing*FAPs*FAP_packing * Arc_AER * Length_AER * Arc_UG * Length_UG +
COF - Demand:FAP_packing:Length_AER:Length_UG -Arc_AER:Length_AER:Arc_UG:Length_UG -
MST_packing:FAPs:Arc_AER:Arc_UG:Length_UG -Demand:FAPs:FAP_packing:Length_AER -
FAP_packing:Length_AER:Length_UG -Demand:MST_packing:FAPs:Length_AER-
FAPs:FAP_packing:Arc_AER:Length_AER:Arc_UG - Demand:FAPs:FAP_packing:Arc_AER:Length_UG -
Demand:FAPs:FAP_packing:Length_AER:Length_UG -Demand:MST_packing:FAPs:Arc_AER:Arc_UG:Length_UG -
Demand:MST_packing:Length_AER:Length_UG - MST_packing:FAPs:FAP_packing:Arc_AER:Length_AER:Arc_UG -
MST_packing:FAPs:Arc_AER:Length_UG - MST_packing:FAPs:FAP_packing:Length_AER:Length_UG -
MST_packing:FAPs:FAP_packing:Length_UG - MST_packing:FAPs:Length_AER:Arc_UG -
Demand:MST_packing:FAPs:Arc_AER:Arc_UG - MST_packing:FAPs:Length_AER:Arc_UG:Length_UG -
Demand:MST_packing:FAPs:FAP_packing:Arc_AER:Arc_UG  - FAPs:Arc_AER:Length_AER -
Demand:FAPs:FAP_packing:Length_AER:Arc_UG - Demand:FAPs:FAP_packing:Arc_UG:Length_UG -
Demand:FAP_packing:Arc_AER:Arc_UG:Length_UG - Demand:MST_packing:Arc_AER:Length_AER:Arc_UG -
MST_packing:FAPs:FAP_packing:Arc_AER:Arc_UG:Length_UG -Demand:MST_packing:FAPs:Arc_AER:Length_AER:Length_UG -
Demand:MST_packing:FAPs:FAP_packing:Arc_AER-Demand:MST_packing:FAPs:FAP_packing:Arc_UG:Length_UG -
FAPs:FAP_packing:Length_AER:Arc_UG:Length_UG -MST_packing:FAPs:FAP_packing:Arc_AER:Length_AER:Length_UG -
MST_packing:FAPs:FAP_packing:Length_AER:Arc_UG -FAPs:FAP_packing:Arc_AER:Arc_UG:Length_UG -
FAPs:FAP_packing:Arc_AER:Length_AER:Length_UG -MST_packing:FAPs:Arc_AER:Length_AER:Arc_UG:Length_UG -
Demand:Arc_AER:Length_AER -Demand:Arc_AER:Length_AER:Length_UG -FAPs:Arc_AER:Arc_UG:Length_UG -
MST_packing:FAPs:Arc_UG -MST_packing:FAPs:Arc_UG -FAPs:FAP_packing:Arc_UG:Length_UG -
Demand:Length_AER-MST_packing:FAP_packing:Arc_AER:Length_UG -Demand:FAP_packing:Length_UG -
FAPs:FAP_packing:Arc_AER:Length_UG -FAPs:Arc_AER:Arc_UG -FAPs:Arc_AER:Length_AER:Arc_UG -
Arc_AER:Length_AER:Length_UG -Demand:MST_packing:Arc_UG:Length_UG -Length_AER -
MST_packing:FAPs:Arc_AER:Length_AER:Length_UG -Demand:MST_packing:FAP_packing:Arc_AER:Length_AER:Arc_UG:Length_UG -
MST_packing:FAPs:Length_UG -Demand:MST_packing:Arc_AER:Length_AER -MST_packing:Arc_AER:Length_UG -
FAPs:Arc_UG:Length_UG -MST_packing:Arc_AER:Length_AER:Arc_UG -Demand:Arc_AER:Length_AER:Arc_UG:Length_UG -
FAPs:Length_AER:Arc_UG -Demand:MST_packing:FAPs -MST_packing:FAPs -Demand:MST_packing:Length_AER -
Demand:MST_packing:FAPs:FAP_packing:Length_AER:Arc_UG:Length_UG - FAPs -
MST_packing:FAPs:FAP_packing:Arc_UG -FAP_packing:Arc_AER -MST_packing:Arc_UG -
FAPs:FAP_packing:Arc_AER:Arc_UG -MST_packing:FAPs:FAP_packing:Arc_AER -MST_packing:FAPs:FAP_packing:Length_AER -
Demand:MST_packing:Arc_AER:Length_UG -Demand:FAP_packing:Arc_AER:Length_AER -
Demand:FAPs:FAP_packing:Arc_AER:Arc_UG:Length_UG -Demand:FAPs:FAP_packing:Arc_AER:Length_AER -
Demand:MST_packing:FAPs:FAP_packing:Arc_AER:Length_AER -MST_packing:Arc_AER - Arc_UG -
MST_packing:FAPs:FAP_packing:Length_AER:Arc_UG:Length_UG -MST_packing:FAP_packing:Arc_AER:Length_AER:Length_UG -
Demand:MST_packing:Arc_AER:Arc_UG -Demand:MST_packing:FAP_packing:Arc_AER:Arc_UG -
MST_packing:FAPs:Length_AER -FAPs:FAP_packing:Length_AER:Arc_UG -
MST_packing:FAPs:FAP_packing:Arc_AER:Arc_UG, data = server_fap)


summary(lm_fap)
par(mfrow = c(2,2))
plot(lm_fap)

pval = summary(lm_fap)$coefficients[,4]
which(pval >0.1)
pval[which(pval == max(pval))]
sort(pval, decreasing = T)[1:40]

#> length(pval)
#[1] 175
server_fap[4,]

predict(lm_fap, new = server_fap[4,], interval = 'prediction')

exp(predict(lm_fap, new = server_fap[4,], interval = 'prediction'))
exp(predict(lm_fap, new = server_fap[4,], interval = 'confidence'))

#        fit      lwr      upr
#25 696.2226 295.6681 1639.426

#> server_fap[4,]
#                                  jobid solve_finish           user         submit_time version       last_modified run_time Demand MSTs MST_packing FAPs FAP_packing FDHs FDH_packing   Dist   COF Arc_AER
#25 07935084-f30e-428a-8b4d-c3a1280ef03b         True justin.carmody 2016-03-24 00:19:42     5.1 2016-03-24T00:33:29    827.0  22719 4135    5.494317  601    7.357737    0           0 No Run Cache    9797
#   Length_AER Arc_UG Length_UG       date runtime
#25   401239.1   5248  171643.4 2016-03-24     827


### FDH solve data only ###


server_fdh = server[which(server$Dist != 'Yes' & server$FDHs != 0),]


pairs20x(log(runtime)~Demand + MSTs + MST_packing + version + FAPs + FAP_packing +FDHs + FDH_packing+ Arc_AER +
Length_AER + Arc_UG + Length_UG + COF, data = server_fdh)


lm_fdh_init = lm(log(runtime)~Demand +MSTs+MST_packing+FAPs+FAP_packing + FDHs+FDH_packing+
              Arc_AER +Length_AER +Arc_UG + Length_UG + COF, data = server_fdh)
summary(lm_fdh_init)

lm_fdh_comp = lm(log(runtime)~Demand*MSTs*MST_packing*FAPs*FAP_packing * FDHs*FDH_packing*
                   Arc_AER *Length_AER *Arc_UG * Length_UG + COF, data = server_fdh)
summary(lm_fdh_comp)





library(MASS)
#lm_fdh_step = stepAIC(lm_fdh_init, scope = lm_fdh_comp,  trace = 12)

lm_fdh_for = lm(log(runtime)~Demand *MSTs *FAPs * FDHs* Arc_AER*Length_AER*Arc_UG * Length_UG +
                   COF + MST_packing *FAP_packing *FDH_packing + Demand:MST_packing *FAP_packing *FDH_packing +
                  MSTs*MST_packing + FAPs*FAP_packing + FDHs*FDH_packing, data = server_fdh)
summary(lm_fdh_for)

dropterm(lm_fdh_for, test = 'F')

lm1 = update(lm_fdh_for, .~. -Demand:MST_packing:FAP_packing:FDH_packing) 
summary(lm1)
dropterm(lm1, test = 'F')

lm2 = update(lm1, .~. -FDHs:FDH_packing ) 
summary(lm2)
dropterm(lm2, test = 'F')

lm3 = update(lm2, .~. -Demand:MSTs:FAPs:FDHs:Arc_AER:Length_AER:Arc_UG:Length_UG ) 
summary(lm3)
dropterm(lm3, test = 'F')

lm4 = update(lm3, .~. -Demand:MSTs:FAPs:Arc_AER:Length_AER:Arc_UG:Length_UG  ) 
summary(lm4)
dropterm(lm4, test = 'F')
lm5 = update(lm4, .~. -Demand:MSTs:FAPs:FDHs:Arc_AER:Arc_UG:Length_UG  ) 
summary(lm5)
dropterm(lm5, test = 'F')
lm6 = update(lm5, .~. -Demand:MSTs:FDHs:Arc_AER:Length_AER:Arc_UG:Length_UG ) 
summary(lm6)
dropterm(lm6, test = 'F')
lm7 = update(lm6, .~. -Demand:MSTs:FAPs:FDHs:Length_AER:Arc_UG:Length_UG  ) 
summary(lm7)
dropterm(lm7, test = 'F')
lm8 = update(lm7, .~. -Demand:MSTs:FAPs:FDHs:Arc_UG:Length_UG  ) 
summary(lm8)
dropterm(lm8, test = 'F')
lm9 = update(lm8, .~. -Demand:MSTs:Arc_AER:Length_AER:Arc_UG:Length_UG  ) 
summary(lm9)
dropterm(lm9, test = 'F')
lm10 = update(lm9, .~. -Demand:MST_packing:FDH_packing ) 
summary(lm10)
dropterm(lm10, test = 'F')

#lm11 = lm10

lm11 = update(lm11,.~. - FAPs:FDHs:Length_AER  )

summary(lm11)$adj.r.squared

pval = summary(lm11)$coefficients[,4]
pval[which(pval >0.05)]

summary(lm11)
Final_model_fdh = summary(lm11)$call

tr = which(rownames(server_fdh) == 241 | rownames(server_fdh) == 762 | rownames(server_fdh) == 601)
server_fdh_no_outlier = server_fdh[-tr,]

lm_fdh_fin = lm(log(runtime) ~ Demand + MSTs + FAPs + FDHs + Arc_AER + 
    Length_AER + Arc_UG + Length_UG + COF + MST_packing + FAP_packing + 
    FDH_packing + Demand:MSTs + Demand:FAPs + MSTs:FAPs + MSTs:FDHs + 
    FAPs:FDHs + MSTs:Arc_AER + FAPs:Arc_AER + FDHs:Arc_AER + 
    Demand:Length_AER + MSTs:Length_AER + FAPs:Length_AER + Demand:Arc_UG + 
    FAPs:Arc_UG + Arc_AER:Arc_UG + Demand:Length_UG + MSTs:Length_UG + 
    FAPs:Length_UG + FDHs:Length_UG + Arc_AER:Length_UG + Arc_UG:Length_UG + 
    MST_packing:FAP_packing + MST_packing:FDH_packing + FAP_packing:FDH_packing + 
    Demand:MST_packing + FAPs:FAP_packing + Demand:MSTs:FAPs + 
    MSTs:FAPs:FDHs + Demand:MSTs:Arc_AER + Demand:FAPs:Arc_AER + 
    Demand:FDHs:Arc_AER + MSTs:FDHs:Arc_AER + FAPs:FDHs:Arc_AER + 
    Demand:MSTs:Length_AER + Demand:FAPs:Length_AER + MSTs:FAPs:Length_AER + 
    Demand:Arc_AER:Length_AER + MSTs:Arc_AER:Length_AER + FDHs:Arc_AER:Length_AER + 
    Demand:FAPs:Arc_UG + MSTs:FAPs:Arc_UG + Demand:FDHs:Arc_UG + 
    MSTs:FDHs:Arc_UG + FAPs:FDHs:Arc_UG + Demand:Arc_AER:Arc_UG + 
    MSTs:Arc_AER:Arc_UG + FAPs:Arc_AER:Arc_UG + FDHs:Arc_AER:Arc_UG + 
    Demand:Length_AER:Arc_UG + MSTs:Length_AER:Arc_UG + FAPs:Length_AER:Arc_UG + 
    FDHs:Length_AER:Arc_UG + Arc_AER:Length_AER:Arc_UG + Demand:MSTs:Length_UG + 
    Demand:FAPs:Length_UG + MSTs:FAPs:Length_UG + Demand:FDHs:Length_UG + 
    FAPs:FDHs:Length_UG + Demand:Arc_AER:Length_UG + MSTs:Arc_AER:Length_UG + 
    FDHs:Arc_AER:Length_UG + Demand:Length_AER:Length_UG + MSTs:Length_AER:Length_UG + 
    FAPs:Length_AER:Length_UG + FDHs:Length_AER:Length_UG + Arc_AER:Length_AER:Length_UG + 
    MSTs:Arc_UG:Length_UG + FAPs:Arc_UG:Length_UG + FDHs:Arc_UG:Length_UG + 
    Arc_AER:Arc_UG:Length_UG + Length_AER:Arc_UG:Length_UG + 
    MST_packing:FAP_packing:FDH_packing + Demand:MST_packing:FAP_packing + 
    Demand:MSTs:FAPs:FDHs + Demand:MSTs:FAPs:Arc_AER + MSTs:FAPs:FDHs:Arc_AER + 
    Demand:MSTs:FAPs:Length_AER + Demand:MSTs:FDHs:Length_AER + 
    MSTs:FAPs:FDHs:Length_AER + Demand:MSTs:Arc_AER:Length_AER + 
    Demand:FAPs:Arc_AER:Length_AER + MSTs:FAPs:Arc_AER:Length_AER + 
    Demand:FDHs:Arc_AER:Length_AER + MSTs:FDHs:Arc_AER:Length_AER + 
    Demand:MSTs:FDHs:Arc_UG + MSTs:FAPs:FDHs:Arc_UG + Demand:FAPs:Arc_AER:Arc_UG + 
    MSTs:FAPs:Arc_AER:Arc_UG + Demand:FDHs:Arc_AER:Arc_UG + MSTs:FDHs:Arc_AER:Arc_UG + 
    FAPs:FDHs:Arc_AER:Arc_UG + Demand:MSTs:Length_AER:Arc_UG + 
    Demand:FAPs:Length_AER:Arc_UG + MSTs:FAPs:Length_AER:Arc_UG + 
    Demand:FDHs:Length_AER:Arc_UG + MSTs:FDHs:Length_AER:Arc_UG + 
    FAPs:FDHs:Length_AER:Arc_UG + Demand:Arc_AER:Length_AER:Arc_UG + 
    FAPs:Arc_AER:Length_AER:Arc_UG + FDHs:Arc_AER:Length_AER:Arc_UG + 
    Demand:MSTs:FAPs:Length_UG + Demand:MSTs:FDHs:Length_UG + 
    Demand:FAPs:FDHs:Length_UG + MSTs:FAPs:FDHs:Length_UG + Demand:FAPs:Arc_AER:Length_UG + 
    MSTs:FDHs:Arc_AER:Length_UG + Demand:MSTs:Length_AER:Length_UG + 
    Demand:FAPs:Length_AER:Length_UG + MSTs:FAPs:Length_AER:Length_UG + 
    Demand:FDHs:Length_AER:Length_UG + MSTs:FDHs:Length_AER:Length_UG + 
    FAPs:FDHs:Length_AER:Length_UG + Demand:Arc_AER:Length_AER:Length_UG + 
    MSTs:Arc_AER:Length_AER:Length_UG + FAPs:Arc_AER:Length_AER:Length_UG + 
    FDHs:Arc_AER:Length_AER:Length_UG + Demand:MSTs:Arc_UG:Length_UG + 
    Demand:FAPs:Arc_UG:Length_UG + MSTs:FAPs:Arc_UG:Length_UG + 
    FAPs:FDHs:Arc_UG:Length_UG + Demand:Arc_AER:Arc_UG:Length_UG + 
    MSTs:Arc_AER:Arc_UG:Length_UG + FAPs:Arc_AER:Arc_UG:Length_UG + 
    Demand:Length_AER:Arc_UG:Length_UG + FAPs:Length_AER:Arc_UG:Length_UG + 
    FDHs:Length_AER:Arc_UG:Length_UG + Arc_AER:Length_AER:Arc_UG:Length_UG + 
    Demand:MSTs:FAPs:FDHs:Arc_AER + Demand:MSTs:FAPs:FDHs:Length_AER + 
    Demand:MSTs:FDHs:Arc_AER:Length_AER + MSTs:FAPs:FDHs:Arc_AER:Length_AER + 
    Demand:MSTs:FAPs:FDHs:Arc_UG + Demand:MSTs:FAPs:Arc_AER:Arc_UG + 
    Demand:MSTs:FDHs:Arc_AER:Arc_UG + Demand:FAPs:FDHs:Arc_AER:Arc_UG + 
    MSTs:FAPs:FDHs:Arc_AER:Arc_UG + Demand:MSTs:FAPs:Length_AER:Arc_UG + 
    Demand:FAPs:FDHs:Length_AER:Arc_UG + MSTs:FAPs:FDHs:Length_AER:Arc_UG + 
    Demand:MSTs:Arc_AER:Length_AER:Arc_UG + Demand:FAPs:Arc_AER:Length_AER:Arc_UG + 
    MSTs:FAPs:Arc_AER:Length_AER:Arc_UG + Demand:FDHs:Arc_AER:Length_AER:Arc_UG + 
    MSTs:FDHs:Arc_AER:Length_AER:Arc_UG + FAPs:FDHs:Arc_AER:Length_AER:Arc_UG + 
    Demand:MSTs:FAPs:Arc_AER:Length_UG + Demand:MSTs:FDHs:Arc_AER:Length_UG + 
    Demand:FAPs:FDHs:Arc_AER:Length_UG + MSTs:FAPs:FDHs:Arc_AER:Length_UG + 
    Demand:MSTs:FAPs:Length_AER:Length_UG + Demand:MSTs:FDHs:Length_AER:Length_UG + 
    Demand:FAPs:FDHs:Length_AER:Length_UG + MSTs:FAPs:FDHs:Length_AER:Length_UG + 
    Demand:MSTs:Arc_AER:Length_AER:Length_UG + MSTs:FAPs:Arc_AER:Length_AER:Length_UG + 
    Demand:FDHs:Arc_AER:Length_AER:Length_UG + MSTs:FDHs:Arc_AER:Length_AER:Length_UG + 
    FAPs:FDHs:Arc_AER:Length_AER:Length_UG + Demand:MSTs:FDHs:Arc_UG:Length_UG + 
    Demand:FAPs:FDHs:Arc_UG:Length_UG + MSTs:FAPs:FDHs:Arc_UG:Length_UG + 
    Demand:MSTs:Arc_AER:Arc_UG:Length_UG + Demand:FAPs:Arc_AER:Arc_UG:Length_UG + 
    Demand:FDHs:Arc_AER:Arc_UG:Length_UG + MSTs:FDHs:Arc_AER:Arc_UG:Length_UG + 
    FAPs:FDHs:Arc_AER:Arc_UG:Length_UG + Demand:FAPs:Length_AER:Arc_UG:Length_UG + 
    Demand:FDHs:Length_AER:Arc_UG:Length_UG + MSTs:FDHs:Length_AER:Arc_UG:Length_UG + 
    FAPs:FDHs:Length_AER:Arc_UG:Length_UG + MSTs:Arc_AER:Length_AER:Arc_UG:Length_UG + 
    FAPs:Arc_AER:Length_AER:Arc_UG:Length_UG + FDHs:Arc_AER:Length_AER:Arc_UG:Length_UG + 
    Demand:MSTs:FAPs:FDHs:Arc_AER:Arc_UG + Demand:MSTs:FAPs:FDHs:Length_AER:Arc_UG + 
    Demand:MSTs:FAPs:Arc_AER:Length_AER:Arc_UG + Demand:MSTs:FDHs:Arc_AER:Length_AER:Arc_UG + 
    Demand:FAPs:FDHs:Arc_AER:Length_AER:Arc_UG + MSTs:FAPs:FDHs:Arc_AER:Length_AER:Arc_UG + 
    Demand:MSTs:FAPs:FDHs:Arc_AER:Length_UG + Demand:MSTs:FDHs:Arc_AER:Length_AER:Length_UG + 
    Demand:FAPs:FDHs:Arc_AER:Length_AER:Length_UG + MSTs:FAPs:FDHs:Arc_AER:Length_AER:Length_UG + 
    Demand:FAPs:FDHs:Arc_AER:Arc_UG:Length_UG + MSTs:FAPs:FDHs:Arc_AER:Arc_UG:Length_UG + 
    Demand:FAPs:FDHs:Length_AER:Arc_UG:Length_UG + MSTs:FAPs:FDHs:Length_AER:Arc_UG:Length_UG + 
    MSTs:FAPs:Arc_AER:Length_AER:Arc_UG:Length_UG + Demand:FDHs:Arc_AER:Length_AER:Arc_UG:Length_UG + 
    MSTs:FDHs:Arc_AER:Length_AER:Arc_UG:Length_UG, data = server_fdh_no_outlier)

summary(lm_fdh_fin)
pval = summary(lm_fdh_fin)$coefficients[,4]

par(mfrow = c(2,2))
plot(lm_fdh_fin)

server_fdh[1,]
exp(predict(lm_fdh_fin, new = server_fdh[1,], interval = 'prediction'))

######################################################################################################################
######################################################################################################################
################ USELESS #############################################################################################

library(MASS)
lm_fdh_step = stepAIC(lm_fdh, trace = 1)
summary(lm_fdh_step)

lm_fdh_step_2 = stepAIC(lm_fdh_step, trace = 1)
summary(lm_fdh_step_2)

lm_fdh_step_3 = stepAIC(lm_fdh_step_2, trace = 1)
summary(lm_fdh_step_3)

lm_fdh_step_4 = stepAIC(lm_fdh_step_3, trace = 1)
summary(lm_fdh_step_4)

lm_fdh_step_5 = stepAIC(lm_fdh_step_4, trace = 1)
summary(lm_fdh_step_5)

lm_fdh_step_6 = stepAIC(lm_fdh_step_5, trace = 1)
summary(lm_fdh_step_6)

lm_fdh_step_7 = stepAIC(lm_fdh_step_6, trace = 1)
summary(lm_fdh_step_7)

#Not working any more
lm_fdh_step_8 = stepAIC(lm_fdh_step_7, trace = 1)
summary(lm_fdh_step_8)

dt = dropterm(lm_fdh_step_7, test = 'F')

pval = summary(lm_fdh_step_7)$coefficients[,4]
pval[which(pval >0.96)]


###SINCE the R squared dropped down to 82 percent, we might as well go back and redo the backward selection, using a simplified model!###



#########################################################################################################################
#########################################################################################################################

sort(pval, decreasing = T)[1:5]




### Dist solve data only ###
server_dist = server[server$Dist == 'Yes',]
tr = which(server_dist$FAP_packing < 5 | server_dist$FDH_packing < 7)
server_dist = server_dist[-tr,]
tr1 = which(rownames(server_dist) == 5010)
server_dist_no_outlier = server_dist[-tr1,]
pairs20x(log(runtime)~Demand + MSTs + MST_packing + version + FAPs + FAP_packing +FDHs + FDH_packing+ Arc_AER +
Length_AER + Arc_UG + Length_UG + COF, data = server_dist)

#which(server_dist$FDH_packing <7)
#server_dist[64,]

lm_dist_init = lm(log(runtime)~Demand +MSTs+MST_packing+FAPs+FAP_packing + FDHs+FDH_packing+
              Arc_AER +Length_AER +Arc_UG + Length_UG + COF, data = server_dist)
summary(lm_dist_init)

lm_dist_comp = lm(log(runtime)~Demand *MSTs *FAPs * FDHs* Arc_AER*Length_AER*Arc_UG * Length_UG +
                   COF + MST_packing *FAP_packing *FDH_packing + Demand:MST_packing *FAP_packing *FDH_packing +
                  MSTs*MST_packing + FAPs*FAP_packing + FDHs*FDH_packing, data = server_dist)
summary(lm_dist_comp)

lm_dist_for = lm(log(runtime)~Demand *FAPs * FDHs* Arc_AER*Length_AER*Arc_UG * Length_UG +
                   COF  , data = server_dist)
summary(lm_dist_for)


lm_d = lm_dist_fin

lm_d = update(lm_d, .~. - FDHs:FAPs)
pval = summary(lm_d)$coefficients[,4]
pval[which(pval >0.10)]

summary(lm_d)$call
lm_dist_fin = lm(formula = log(runtime) ~ FDHs + Length_AER + Arc_UG + Length_UG + 
    COF + Demand:FAPs + FDHs:Demand + Demand:Arc_AER + FDHs:Arc_AER + 
    Length_AER:FAPs + Length_AER:Arc_AER + Arc_UG:Demand + Arc_UG:FAPs + 
    FDHs:Arc_UG + Arc_UG:Arc_AER + Length_AER:Arc_UG + Length_UG:Demand + 
    Length_UG:FAPs + FDHs:Length_UG + Length_AER:Length_UG + 
    Arc_UG:Length_UG + Demand:FAPs:Arc_AER + FDHs:Demand:Arc_AER + 
    FDHs:FAPs:Arc_AER + Length_AER:Demand:FAPs + FDHs:Length_AER:FAPs + 
    Length_AER:Demand:Arc_AER + FDHs:Length_AER:Arc_AER + Arc_UG:Demand:FAPs + 
    FDHs:Arc_UG:Demand + FDHs:Arc_UG:FAPs + Arc_UG:Demand:Arc_AER + 
    Arc_UG:FAPs:Arc_AER + Length_AER:Arc_UG:Demand + Length_AER:Arc_UG:FAPs + 
    FDHs:Length_AER:Arc_UG + Length_AER:Arc_UG:Arc_AER + Length_UG:Demand:FAPs + 
    FDHs:Length_UG:Demand + FDHs:Length_UG:FAPs + Length_UG:Demand:Arc_AER + 
    Length_UG:FAPs:Arc_AER + FDHs:Length_UG:Arc_AER + Length_AER:Length_UG:Demand + 
    FDHs:Length_AER:Length_UG + Length_AER:Length_UG:Arc_AER + 
    Arc_UG:Length_UG:Demand + Arc_UG:Length_UG:FAPs + FDHs:Arc_UG:Length_UG + 
    FDHs:Demand:FAPs:Arc_AER + FDHs:Length_AER:Demand:FAPs + 
    Length_AER:Demand:FAPs:Arc_AER + FDHs:Length_AER:Demand:Arc_AER + 
    FDHs:Arc_UG:Demand:FAPs + FDHs:Arc_UG:Demand:Arc_AER + FDHs:Arc_UG:FAPs:Arc_AER + 
    Length_AER:Arc_UG:Demand:FAPs + FDHs:Length_AER:Arc_UG:Demand + 
    FDHs:Length_AER:Arc_UG:FAPs + Length_AER:Arc_UG:Demand:Arc_AER + 
    Length_AER:Arc_UG:FAPs:Arc_AER + FDHs:Length_UG:Demand:FAPs + 
    FDHs:Length_UG:Demand:Arc_AER + FDHs:Length_UG:FAPs:Arc_AER + 
    Length_AER:Length_UG:Demand:FAPs + FDHs:Length_AER:Length_UG:FAPs + 
    Length_AER:Length_UG:Demand:Arc_AER + Length_AER:Length_UG:FAPs:Arc_AER + 
    Arc_UG:Length_UG:Demand:FAPs + Arc_UG:Length_UG:Demand:Arc_AER + 
    Arc_UG:Length_UG:FAPs:Arc_AER + FDHs:Arc_UG:Length_UG:Arc_AER + 
    Length_AER:Arc_UG:Length_UG:Demand + Length_AER:Arc_UG:Length_UG:FAPs + 
    FDHs:Length_AER:Arc_UG:Length_UG + FDHs:Length_AER:Demand:FAPs:Arc_AER + 
    FDHs:Arc_UG:Demand:FAPs:Arc_AER + Length_AER:Arc_UG:Demand:FAPs:Arc_AER + 
    FDHs:Length_AER:Arc_UG:Demand:Arc_AER + FDHs:Length_AER:Arc_UG:FAPs:Arc_AER + 
    FDHs:Length_AER:Length_UG:FAPs:Arc_AER + FDHs:Arc_UG:Length_UG:Demand:FAPs + 
    FDHs:Arc_UG:Length_UG:Demand:Arc_AER + Length_AER:Arc_UG:Length_UG:Demand:FAPs + 
    Length_AER:Arc_UG:Length_UG:Demand:Arc_AER + FDHs:Length_AER:Arc_UG:Length_UG:Arc_AER + 
    FDHs:Length_AER:Arc_UG:Demand:FAPs:Arc_AER + FDHs:Arc_UG:Length_UG:Demand:FAPs:Arc_AER + 
    Length_AER:Arc_UG:Length_UG:Demand:FAPs:Arc_AER, data = server_dist_no_outlier)


summary(lm_dist_fin)

par(mfrow = c(2,2))
plot(lm_dist_fin)



server_dist[6,]

exp(predict(lm_dist_fin, new = server_dist[6,], interval = 'prediction'))


standard_hut = server_mst[6,]
standard_hut[8:14] =c(26500,6000,5.0, 857,7,78,11)
standard_hut[17:20] = c(15000, 560000, 1500, 55000)

mst_fake = c(standard_hut[8:10], standard_hut[17:20])
predict(lm_mst_fin, new = mst_fake, interval = 'prediction')
exp(predict(lm_mst_fin, new = mst_fake, interval = 'prediction'))
exp(predict(lm_mst_fin, new = server_dist[6,], interval = 'prediction'))
exp(predict(lm_fap, new = standard_hut, interval = 'prediction'))

# Change df user format to firstname.surname #
head(df$Users)
df_Users = c()
for (d1 in 1: length(df$Users))
{
  tmp_cha = strsplit(as.character(df$Users[d1]), split = ' ')
  df_Users[d1] =  paste(tolower(tmp_cha[[1]][1]),tolower(tmp_cha[[1]][2]), sep = '.' )
}
df$Users = df_Users

runtime = c()
library(lubridate)
for (u1 in 1: dim(df)[1])
{
  #date_df = as.Date(df$Start_Time[u1])
  date_df = strsplit(as.character(df$Start_Time[u1]), split = ',')

  #hours_taken = make_difftime(df$Booked_Time[u1]/8 * 24 * 60*60, units = 'hour')
  #date_end = date_df + hours_taken
  user_df = as.character(df$Users.group[u1])
  tmp_char = strsplit(user_df, split = ',')
  loc = c()

  for (c1 in 1: length(tmp_char[[1]]))
  {
    name_char = strsplit(as.character(tmp_char[[1]][c1]), split = ' ')
    one_user = paste(tolower(name_char[[1]][1]),tolower(name_char[[1]][2]), sep = '.' )
    if(one_user == 'alex.brazel')
    {
      one_user = 'alexander.brazel'
    }
    for (st1 in 1: length(date_df[[1]]))
    {
      one_day = date_df[[1]][st1]
      one_loc = which(as.Date(server$date) == one_day & server$user == one_user & server$run_time != 'No info') 
      loc = c(loc,one_loc)
    }
    # one_loc = which(as.Date(server$date) >= date_df &
    #               as.Date(server$date) <= date_end &
    #               server$user == one_user & server$run_time != 'No info')
    
  }
  
  runtime[u1] = sum(as.numeric(server$run_time[loc]))
}



##############################################################################################
### INTERESTING OBSERVATIONS ###
user = unique(server$user)
user.run = c()
for (i in 1: length(user))
{
	user.run[i] = length(which(server$user == user[i]))
}

user.run

user[order(user.run, decreasing = T)]
user.run[order(user.run, decreasing = T)]


