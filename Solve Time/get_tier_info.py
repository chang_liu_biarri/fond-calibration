
import json
import re
def get_log(data):
    some_info = {}    

    Demand = MSTs = MST_packing = FAPs = FAP_packing = FDHs = FDH_packing = Dist = 'No Run'
    for i in data[-12:-1]:

        #xjs = i.replace('"', '\"')
        #xjs = i[:-1]
        xjs = re.sub(',\n','',i)
        get_tier = json.loads(xjs)
        tier_msg = get_tier.get('message','No info')
        demandcount = re.split("Demands: ", tier_msg)
        mstcount = re.split("Drop terminals\(MSTs\): ", tier_msg)
        fapcount = re.split("NAPs\(FAPs\): ", tier_msg)
        fdhcount = re.split("HUBs\(FDHs\): ", tier_msg)
        mstpack = re.split("MST Packing Ratio: ", tier_msg)
        fappack = re.split("FAP Packing Ratio: ", tier_msg)
        fdhpack = re.split("FDH Packing Ratio: ", tier_msg)
        
        if len(demandcount) > 1:
            Demand = demandcount[1]

        if len(mstcount) > 1:
            MSTs = mstcount[1]

        if len(mstpack) > 1:
            MST_packing = mstpack[1]
        #    MSTs = 'No Run'
        #    MST_packing = 'No Run' 
        if len(fapcount) > 1:
            FAPs = fapcount[1]
        
        if len(fappack)> 1:
            FAP_packing = fappack[1]
        #else:
        #    FAPs = 'No Run'
        #    FAP_packing = 'No Run'
        if len(fdhcount) > 1:
            FDHs = fdhcount[1]
        if len(fdhpack) > 1:
            FDH_packing = fdhpack[1]
        #else:
        #    FDHs = 'No Run'
        #    FDH_packing = 'No Run'

    
    # packtier = re.split(':?', tier_msg)
    # performtier = re.split("Not performing", tier_msg)
    # if len(packtier) == 1 and len(performtier) > 1:
        # tier_packing = 'No Run'
    # elif len(packtier) == 2:
        # tier_packing = packtier[1]
    # else:
        # tier_packing = 'No info'
    # some_info.append(tier_packing
    
    #last_line = data[-1][:-1]
    last_line = re.sub(',\n','',data[-1])
    ljs = last_line.replace('"', '\"')
    lldict = json.loads(ljs)
    ll_msg = lldict.get("message",'No info')
    if ll_msg[-3:] == 'HUT':
        Dist = 'Yes'
    
    #cf = data[3][:-1]
    cf = re.sub(',\n','',data[3])
    cfj = cf.replace('"', '\"')
    cachefp = json.loads(cfj)
    cf_msg = cachefp.get("message",'No info')
    cf_use = re.split('Detected the provided cache as ',cf_msg)[1]
    if re.search('no solution', cf_msg):
        cof = 'Footprint'
    else:
        cof = 'Cache'
        
    some_info["Demand"] = Demand
    some_info["MSTs"] = MSTs
    some_info["MST_packing"] = MST_packing
    some_info["FAPs"] = FAPs
    some_info["FAP_packing"] = FAP_packing
    some_info["FDHs"] = FDHs
    some_info["FDH_packing"] = FDH_packing
    some_info["Dist"] = Dist
    some_info["COF"] = cof
    
    return some_info
    
    
if __name__ == '__main__':
    f1 = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log\log.txt", 'r')
    f2 = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_2\log.txt", 'r')
    f3 = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_3\log.txt", 'r')
    f4 = open("C:\Users\chang\Documents\DS work\Solve Time\Example Jenkins Log_4\log.txt", 'r')
    x1 = f1.readlines()
    x2 = f2.readlines()
    x3 = f3.readlines()
    x4 = f4.readlines()
    Kyle1 = get_log(x1)
    Kyle2 = get_log(x2)
    Kyle3 = get_log(x3)
    Kyle4 = get_log(x4)
    print Kyle1, Kyle2, Kyle3, Kyle4
    
    
    
    
    
    
    
    
    
    
    